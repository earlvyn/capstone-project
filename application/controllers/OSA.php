<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OSA extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('email');
	}

	public function Osa_class()
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-form');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_form_daily() // Daily Attendance Monitoring Form
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-form-daily');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_pending_forms_daily() // Daily Attendance Monitoring Form Pending Forms
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-pending-form-daily');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_approved_forms_daily() // Daily Attendance Monitoring Form Approve Forms
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-approved-form-daily');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_approved_form_warning() // Warning Approve Forms
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-approved-form-warning');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_accept_daily() // Daily Attendance Monitoring Form Pending Forms
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-accept-daily');
				$this->load->view('templates/footer_osa');	
			}		
	}



	public function Submit_form_daily() // Submitted forms
		{

			if(isset($_POST['insert']))
			{
			$code = $_GET['code']; // ClassCode
			$date = $_GET['date']; // Date
	
			$osa_st = '1';

			$qwe = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

			$q = $qwe->row();

			$id = $q->Teacher_FK;

			$paw = $this->db->query("SELECT * FROM forms WHERE ClassCode='$code' AND `Date`='$date' AND Teacher_fk='$id'");

			foreach($paw->result_array() as $p)
			{
				$stat = $p['Remark']; // Remark

				if($stat == 'Absent' || $stat == 'Late')
				{
					$id2 = $p['Students_fk']; //Students_fk

					$this->db->query("UPDATE forms SET osa_status='$osa_st' where ClassCode='$code' AND Students_fk='$id2' AND `Date`='$date'");
				}
			}

			echo '<script>alert("Form Successfully Received!");</script>';

			redirect(base_url() . 'osa/osa_pending_forms_daily', 'refresh');
			
			}
		}


	public function Osa_view_daily() // View Approved Forms
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-view-daily');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_view_warning() // View Approved Forms warning
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-view-warning');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_form_warning() // Warning form option
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-form-warning');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_form_af() // AF Form Option
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-form-af');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_pending_form_warning() // Warning Pending Forms
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-pending-form-warning');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Osa_accept_warning() // Warning Pending Forms
	{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			else
			{
				$data['username'] = "OSA";

				$this->load->view('templates/header_osa', $data);
				$this->load->view('osa/osa-accept-warning');
				$this->load->view('templates/footer_osa');	
			}		
	}

	public function Submit_form_warning() // Submitted forms warning
		{

			if(isset($_POST['insert']))
			{
			$code = $_GET['code']; // ClassCode
			$date = $_GET['date']; // Date
			$idn = $_GET['id'];
	
			$osa_st = '3';

			$qwe = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

			$q = $qwe->row();

			$id = $q->Teacher_FK;
				
			$this->db->query("UPDATE forms SET osa_status='$osa_st' where ClassCode='$code' AND Students_fk='$idn' AND `Date`='$date' AND Form_Type='warning' AND osa_status='2'");
				
			echo '<script>alert("Form Successfully Received!");</script>';

			redirect(base_url() . 'osa/osa_pending_form_warning', 'refresh');
			
			}
		}


}
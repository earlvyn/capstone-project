<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	class Login extends CI_Controller 
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->helper('form');
			$this->load->helper('email');	
		}

		public function index() //login page
		{	
			//Check session if set or not
			if(!isset($_SESSION['username']))
			{
				$data['title'] = "AAMS";
				$this->load->view('templates/header', $data); //load header of the login page
				$this->load->view('login_view'); //login content of the login page
				$this->load->view('templates/footer');	//load footer of the login page
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}					
		}

		public function submit_loginuser() // Login validation
		{
			//Form validation of username and password
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required'); 

			if($this->form_validation->run() == TRUE)
			{
				$username = stripslashes($this->input->post('username'));
				$password = stripslashes($this->input->post('password')); 

				$this->load->model('login_model');
				$user_id = $this->login_model->validate_login($username, $password);

				if($user_id)
				{
					$role = $this->db->get_where('users', array('username' => $username, 'password' => $password))->row();

					$session_data = array('username' => $username, 'role'=> $role->role);

					$this->session->set_userdata($session_data);
					
					
					if($this->session->userdata('username') == true && $_SESSION['role'] == 0) //Student Page
					{
						
						$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

						if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
						{
							redirect(base_url('students/editprofile'));
						}
						else
						{
							if(!isset($_SESSION['username']))
							{
								redirect(base_url());
							}
							else
							{
								redirect(base_url('students/students_class'));	
							}
							
						}
						
					}
					elseif($this->session->userdata('username') == true && $_SESSION['role'] == 1) //Teacher Page
					{
						$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

						if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
						{
							redirect(base_url('teachers/editprofile'));
						}
						else
						{
							if(!isset($_SESSION['username']))
							{
								redirect(base_url());
							}
							else
							{
								redirect(base_url('teachers/teachers_class'));
							}
							
						}
					}
					elseif($this->session->userdata('username') == true && $_SESSION['role'] == 2) //OSA page
					{
						if(!isset($_SESSION['username']))
						{
							redirect(base_url());
						}
						else
						{
							redirect(base_url('osa/osa_class'));
						}	
					}
					else
					{
						redirect(base_url());
					}
				}
				else
				{
	
					$this->session->set_flashdata('error_login', "<div style='color: red'>Invalid Username or Password </div>");

					redirect(base_url());
				}
			}
			else
			{
				//$data = array('error_login' => validation_errors(), 'username' => $this->input->post('username'));

				//$this->session->set_flashdata($data);
				redirect(base_url());
			}



		}

		public function logout() // logout function
		{
			$this->session->unset_userdata('username');
			$this->session->sess_destroy();
			redirect(base_url() . 'login');
		}

		public function register() // register function
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('idnumber', 'ID-Number', 'trim|min_length[10]|max_length[10]|callback_idcheck|is_unique[users.Username]');
			$this->form_validation->set_rules('emailsignup', 'Email', 'trim|valid_email|is_unique[students.Email]'); 
			$this->form_validation->set_rules('passwordsignup', 'Password', 'trim|required');
			$this->form_validation->set_rules('passwordsignup_confirm', 'Password Confirmation', 'trim|required|matches[passwordsignup]');

			if($this->form_validation->run() == FALSE) //didnt validate
			{
				//Send data to retain message in the input field
				$data = array('error_signup' => validation_errors(), 'idnumber' => $this->input->post('idnumber'), 'emailsignup' => $this->input->post('emailsignup'));

				$this->session->set_flashdata($data);
				redirect(base_url() . 'login#toregister'); //login content of the login page
			
			}
			else
			{
				$this->load->model('login_model');

				if($query = $this->login_model->validate_register())
				{
					echo '<script>alert("You Have Successfully Created an account!");</script>';
					$data['title'] = "AAMS";
					$this->load->view('templates/header', $data); //load header of the login page
					$this->load->view('login_view'); //login content of the login page
					$this->load->view('templates/footer');	//load footer of the login page	);
				}
				else
				{
					echo '<script>alert("Try Again!");</script>';
					redirect(base_url() . 'login#toregister'); //login content of the login page
				}

			}
		}

		public function idcheck($idnumber) // Call back function for register
		{

			$first_ch = substr($idnumber,0,1);
			//$second_ch = substr($idnumber,1,10);
			if($first_ch == 's' || $first_ch == 'S' || $first_ch == 'p' || $first_ch == 'P')
			{
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('idcheck','*Only s or p are allowed in the username plus the idnumber ex. s201310183 or p201310183');
            	return FALSE;
			}
		}


	}
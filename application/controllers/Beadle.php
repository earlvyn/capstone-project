<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beadle extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('email');
	}

	public function Beadle_attendance() // Display Beadle Attendance
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-attendance');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Beadle_list() // Display Class List
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-list');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Beadle_generate() // generate pdf file
		{

			$this->load->library('Pdf');
			$this->load->view('beadle/makepdf');
		}

	public function Beadle_attendance_option() // Attendance Option
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-attendance-option.php');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Beadle_attendance_add() // Attendance Add Record List View
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-attendance-add');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Beadle_sp_attendance() // Record Attendance in seat plan view
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-sp-attendance');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Beadle_sp() // View Seat plan
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-sp');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Beadle_config() // Config Seat Plan
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-config');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}


	public function Record_attendance() // Record attendance in beadle view
		{
			date_default_timezone_set('Asia/Manila');

			$current_date = date("Y-m-d");
			$time = date("g:i:s A");

			if(isset($_POST['insert']))
			{
		
				$code = $_GET['code'];
				
				$ide = $this->session->userdata("username");

				$result = $this->db->query("SELECT Student_ID, Teacher_FK FROM students, classes WHERE Students_FK=Student_ID AND ClassCode_FK='$code' ORDER BY Last_Name ASC");
					
				$i = 0;
					
				foreach($result->result_array() as $row)
				{
					
				$id = $row['Student_ID'];
				$teacher = $row['Teacher_FK'];

				$Status = $_POST['Status' . $i++];

				
				$add_record = array('Date' => $current_date, 'time' => $time, 'Status' => $Status, 'ClassCode_fk' => $code, 'Students_fk' => $id, 'Teacher_fk' => $teacher, 'Recorded_by' => $ide);

				$subj = $this->db->insert('attendance_record', $add_record);
				

				}
				echo '<script>alert("You Have Successfully Recorded an Attendance!");</script>';
				redirect(base_url() . 'beadle/beadle_attendance?code=' . $code, 'refresh');
				
			}
		}


		public function configure_sp()
		{
			$a_col = $_POST['lcol'];
			$a_row= $_POST['lrow'];
			$b_col = $_POST['rcol'];
			$b_row = $_POST['rrow'];
			$code = $_GET['code'];

			$query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

			foreach($query->result_array() as $rows)
			{
				$sid = $rows['Students_FK'];
				$code1 = $rows['ClassCode_FK'];
				$tk = $rows['Teacher_FK'];


				$add_sp = array('arow' => $a_row, 'acol' => $a_col, 'brow' => $b_row, 'bcol' => $b_col);

				$this->db->update('classes', $add_sp, array('ClassCode_FK' => $code1, 'Students_FK' => $sid, 'Teacher_FK' => $tk));

			}

			echo '<script>alert("You Have Successfully Set a seat!");</script>';

			redirect(base_url() . 'beadle/beadle_config?code=' . $code, 'refresh');
		}

		public function assign_seat() // Function inside seat plan assigning seat
		{
			if(isset($_POST['save']))
			{
				$code = $_GET['code'];
				$name = $_POST['name'];
				$x = $_POST['id2'];

				$this->db->query("UPDATE classes set seat_no = '$x' where Students_FK = '$name' AND ClassCode_FK='$code'");

				echo '<script>alert("You Have Successfully Assign a seat!");</script>';

				redirect(base_url() . 'beadle/beadle_config?code=' . $code, 'refresh');

			}
		
		}

		public function Record_attendance_sp() // Record attendance in seat plan view
		{
			date_default_timezone_set('Asia/Manila');

			$current_date = date("Y-m-d");
			$time = date("g:i:s A");

			if(isset($_POST['insert']))
			{
		
				$ide = $this->session->userdata("username");
		        $code = $_GET['code'];
		        $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

		        foreach($query->result_array() as $rows)
		        {
		          $acol = $rows['acol'];
		          $arow = $rows['arow'];
		        }
		       
		        $x = 0;
		        
		        for ($row=0; $row < $arow; $row++) 
		        { 
		            for ($col = 0; $col < $acol; $col ++) 
		            	{ 
		              		$x++;

		              		$query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code' AND seat_no='$x'");

		              		$rowz = $query->unbuffered_row('array');

		              		if($x == $rowz['seat_no'])
		              		{
		                		$idn = $rowz['Students_FK'];
		                		$tk = $rowz['Teacher_FK'];

		                		$Status = $_POST['Status' . $x];

		                		$add_record = array('Date' => $current_date, 'time' => $time, 'Status' => $Status, 'ClassCode_fk' => $code, 'Students_fk' => $idn, 'Teacher_fk' => $tk, 'Recorded_by' => $ide);

								$subj = $this->db->insert('attendance_record', $add_record);
							
							}

						}

				}

				$code = $_GET['code'];
       			$query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

		        foreach($query->result_array() as $rows)
		        {
		          	$arow = $rows['arow'];
		          	$acol = $rows['acol'];

		          	$bcol = $rows['bcol'];
		          	$brow = $rows['brow'];
		        }

			        $w = $acol*$arow+1;
			        $i = $brow * $bcol + $w;
			        $x = $i;
			        for ($row=0; $row < $brow;$row++) 
		         	{ 
		   				for ($col = 0; $col < $bcol; $col ++) 
		            		{ 
		              			$x--;
		              
				              	$query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code' AND seat_no='$x'");

				              	$rowz = $query->unbuffered_row('array');

				              	if($x == $rowz['seat_no'])
				              	{
					                $idn = $rowz['Students_FK'];
					                $tk = $rowz['Teacher_FK'];

					                $Status = $_POST['Status' . $x];

					                $add_record = array('Date' => $current_date, 'time' => $time, 'Status' => $Status, 'ClassCode_fk' => $code, 'Students_fk' => $idn, 'Teacher_fk' => $tk, 'Recorded_by' => $ide);

									$subj = $this->db->insert('attendance_record', $add_record);

				             	}
		       				 }

		    			}
					
			}

				echo '<script>alert("You Have Successfully Recorded an Attendance!");</script>';
				redirect(base_url() . 'beadle/beadle_attendance?code=' . $code, 'refresh');
		}


		public function Beadle_attendance_view() // Display Beadle Attendance view
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-attendance-view');
				$this->load->view('templates/footer_students');	
			}	
		}


		
					
	}

	public function Beadle_attendance_edit() // Display Beadle Attendance edit
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-attendance-edit');
				$this->load->view('templates/footer_students');	
			}	
		}
					
	}

	public function Update_attendance() // Update attendance record in beadle view
		{
			if(isset($_POST['update']))
			{
				
				$code = $_GET['code'];
				$date = $_GET['date'];

				$query = $this->db->query("SELECT * from attendance_record where ClassCode_fk = '$code' AND `Date`='$date'");
				$i = 0;
				foreach($query->result_array() as $row)
				{
					$x = $_POST['Status' . $i++];
					$id = $row['Students_fk'];

					$this->db->query("UPDATE attendance_record SET Status='$x' where ClassCode_fk='$code' AND Students_fk='$id' AND `Date`='$date'");

				}
				echo '<script>alert("You Have Successfully Updated Attendance Record!");</script>';
				redirect(base_url() . 'beadle/beadle_attendance_edit?code=' . $code . '&date=' . $date, 'refresh');
			}
				
			
		}


		public function Beadle_daily_form() // Display Beadle daily form
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-daily-form');
				$this->load->view('templates/footer_students');	
			}	
		}
					
	}


	public function Submit_form_daily() // Submitted forms
		{

			if(isset($_POST['insert']))
			{
			$code = $_GET['code']; // ClassCode
			$date = $_GET['date']; // Date
			$id = $this->session->userdata('username'); //Teacher_fk and Recorded by
			$daily = 'daily'; // Form Type
			$appr = 'Pending'; //Status

			$paw = $this->db->query("SELECT * FROM attendance_record WHERE ClassCode_fk='$code' AND `Date`='$date'");

			foreach($paw->result_array() as $p)
			{
				$stat = $p['Status']; // Remark
				$tk = $p['Teacher_fk'];

				if($stat == 'Absent' || $stat == 'Late')
				{
					$id2 = $p['Students_fk']; //Students_fk
					
					$add_form = array('Date' => $date, 'ClassCode' => $code, 'Remark' => $stat, 'Form_Type' => $daily, 'Teacher_fk' => $tk, 'Students_fk' => $id2, 'Status' => $appr, 'Recorded_by' => $id);

					$form = $this->db->insert('forms', $add_form);
				}
			}

			echo '<script>alert("You Have Successfully Send form to Teacher Wait for the approval!!");</script>';

			redirect(base_url() . 'beadle/beadle_attendance?code=' . $code, 'refresh');
			
			}
		}

		public function Beadle_daily_form_view() // Display Beadle daily form view
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-daily-form-view');
				$this->load->view('templates/footer_students');	
			}	
		}
					
	}


	public function Beadle_issue_forms() // Display Beadle issue form
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-issue-forms');
				$this->load->view('templates/footer_students');	
			}	
		}
					
	}

	public function Beadle_issue_forms_warning() // Display Beadle issue form warning
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-issue-forms-warning');
				$this->load->view('templates/footer_students');	
			}	
		}
					
	}

	public function Beadle_issue_forms_warning_create() // Display Beadle Create form warning
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-issue-forms-warning-create');
				$this->load->view('templates/footer_students');	
			}	
		}
					
	}

	public function Submit_form_warning() // Submitted forms warning
		{	
			date_default_timezone_set('Asia/Manila');

			if(isset($_POST['insert']))
			{
			$code = $_GET['code']; // ClassCode
			$date = date("Y-m-d"); // Date
			$idn = $_GET['id']; // Students_fk
			$id = $this->session->userdata('username'); //Teacher_fk and Recorded by
			$daily = 'warning'; // Form Type
			$appr = 'Pending'; //Status
			$stat = 'Warning'; // Remark

			$sql = $this->db->query("SELECT * from subjects WHERE ClassCode='$code'");

			$rows = $sql->row();

			$tk = $rows->Teacher_FK;
			
					
			$add_form = array('Date' => $date, 'ClassCode' => $code, 'Remark' => $stat, 'Form_Type' => $daily, 'Teacher_fk' => $tk, 'Students_fk' => $idn, 'osa_status' => 2, 'Status' => $appr, 'Recorded_by' => $id);

			$form = $this->db->insert('forms', $add_form);

			echo '<script>alert("You Have Successfully Send form to OSA!");</script>';

			redirect(base_url() . 'beadle/beadle_issue_forms_warning?code=' . $code, 'refresh');
			
			}
		}


	public function Beadle_issue_forms_af() // Display Beadle issue form af
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('beadle/beadle-issue-forms-af');
				$this->load->view('templates/footer_students');	
			}	
		}
					
	}






}
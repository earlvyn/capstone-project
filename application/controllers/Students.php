<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('email');
	}

	// Display Student home/class
	public function Students_class()
	{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('students/students-class');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	//Edit Profile for Students
		public function Editprofile()
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					$data['title'] = "AAMS";

					$this->load->view('templates/header_edit_profile', $data); //load header of edit profile page
					$this->load->view('students/edit_profile');
					$this->load->view('templates/footer');	//load footer of the profile page	);
				}
				else
				{
					redirect(base_url('students/students_class'));
				}

				
			}
			
		}

		//Update Profile for Students
		public function Update() 
		{
			if(isset($_POST['update'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('fname', 'First Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('mname', 'Middle Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('lname', 'Last Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('year', 'Year', 'trim|required|numeric|less_than[6]|min_length[1]');
			$this->form_validation->set_rules('course', 'Course', 'trim|required');

			if($this->form_validation->run() == FALSE) //didnt validate
			{
				$this->session->set_flashdata('error_update', validation_errors());
				redirect(base_url() . 'students/editprofile'); //login content of the login page
			}
			else
			{

				$this->load->model('students_model');

				if($query = $this->students_model->update_students())
				{
					echo '<script>alert("You Have Successfully Update your account!");</script>';

					redirect(base_url() . 'students/students_class', 'refresh');
				}
				else
				{
					echo '<script>alert("Try Again!");</script>';
					redirect(base_url() . 'students/editprofile', 'refresh'); //login content of the login page
				}
			}
			}
			

		}

		public function Update_profile_students()
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
			{
				redirect(base_url('teachers/teachers_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('students/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$data['title'] = "AAMS";

					$this->load->view('templates/header_edit_profile', $data); //load header of edit profile page
					$this->load->view('students/profile');
					
				}

				
			}
			
		}

		public function Update_profile1() 
		{
			
			
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('fname', 'First Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('mname', 'Middle Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('lname', 'Last Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('year', 'Year', 'trim|required|numeric|less_than[6]|min_length[1]');
			$this->form_validation->set_rules('course', 'Course', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if($this->form_validation->run() == FALSE) //didnt validate
			{
				$this->session->set_flashdata('error_update', validation_errors());
				redirect(base_url() . 'students/update_profile_students'); //login content of the login page
			}
			else
			{

				$this->load->model('students_model');

				if($query = $this->students_model->update_students())
				{
					echo '<script>alert("You Have Successfully Update your account!");</script>';

					redirect(base_url() . 'students/update_profile_students', 'refresh');
				}
				else
				{
					echo '<script>alert("Try Again!");</script>';
					redirect(base_url() . 'students/update_profile_students', 'refresh'); //login content of the login page
				}
			}
			
			

		}

	// Inserting a class
		public function Add_class()
		{
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('code1', 'Class Code', 'trim|alpha_numeric|required|min_length[6]|max_length[6]');

			if($this->form_validation->run() == FALSE) //didnt validate
			{
				$this->session->set_flashdata('error_update', validation_errors());
				redirect(base_url() . 'students/students_class'); //login content of the login page
			}
			else
			{
				$code1 = $this->input->post('code1');

				$this->load->model('students_model');

				$class_id = $this->students_model->validate_class($code1);

				if($class_id)
				{
					if($query = $this->students_model->enroll_class())
					{
						echo '<script>alert("You Have Successfully Enrolled in a Subject!");</script>';

						redirect(base_url() . 'students/students_class', 'refresh'); 
					}
					else
					{
							$this->session->set_flashdata('error_update', "<div style='color: red'>You have Already Enrolled in the Subject!</div>");

						redirect(base_url() . 'students/students_class'); 
					}
				}
				else
				{
					$this->session->set_flashdata('error_update', "<div style='color: red'>Subject Key Not Found! Try Again!</div>");

					redirect(base_url() . 'students/students_class');
				}
			}
		}


		public function Students_attendance()
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('students/students-attendance');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Students_list()
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('students/students-list');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Students_sp()
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('students/students-sp');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Upload_btn()
		{
			$code = $_GET['code'];
			$date = $_GET['date'];
			$id = $this->session->userdata('username');

		
			$msg = "";

			if(isset($_POST['upload']))
			{
				$target = "images/".basename($_FILES['image']['name']);

				$image = $_FILES['image']['name'];

				$this->db->query("UPDATE attendance_record SET Excuse_letter='$image' where ClassCode_fk='$code' AND Students_fk='$id' AND `Date`='$date'");

				if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) 
				{
				  	$msg = "Image uploaded successfully";
				}
				else
				{
				  	$msg = "Failed to upload image";
				}
			}

			redirect(base_url() . 'students/students_attendance?code=' . $code, 'refresh');
			
			
		}

		public function Students_forms()
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('students/students-forms');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Students_forms_view()
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('students/students-forms-view');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

	public function Students_forms_view1()
		{
		// Check if session is not set
		if(!isset($_SESSION['username'])) 
		{
			redirect(base_url());
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 1)
		{
			redirect(base_url('teachers/teachers_class'));
		}
		elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
		{
			redirect(base_url('osa/osa_class'));
		}
		else
		{

			$username = $this->session->userdata('username');
			
			$name = $this->db->get_where('students', array('Student_ID' => $username))->row();

			if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
			{
				redirect(base_url('students/editprofile'));
			}
			else
			{
				$try = $this->session->userdata('username');

				$name = $this->db->get_where('students', array('Student_ID' => $try))->row();

				$data['username'] = $name->First_Name;

				$this->load->view('templates/header_students', $data);
				$this->load->view('students/students-forms-view1');
				$this->load->view('templates/footer_students');	
			}	
		}
		
					
	}

}
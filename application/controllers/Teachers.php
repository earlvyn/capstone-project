<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	class Teachers extends CI_Controller 
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->helper('form');
			$this->load->helper('email');
			$this->load->model('teachers_model');
		}

		public function Teachers_class() // Teacher Class/Home view
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}

				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-class');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Editprofile() // Teacher Edit Profile View
		{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher details are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					$data['title'] = "AAMS";

					$this->load->view('templates/header_edit_profile', $data); 
					$this->load->view('teachers/edit-profile-teacher');
					$this->load->view('templates/footer');		
				}
				else
				{
					redirect(base_url('teachers/teachers_class'));
				}

				
			}
			
		}

		public function Update() //Update Profile for Teachers after login 
		{
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('fname', 'First Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('mname', 'Middle Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('lname', 'Last Name', 'trim|alpha_numeric_spaces|required|max_length[30]');

			if($this->form_validation->run() == FALSE) 
			{
				$this->session->set_flashdata('error_update', validation_errors());
				redirect(base_url() . 'teachers/editprofile'); 
			}
			else
			{

				$this->load->model('teachers_model');

				if($query = $this->teachers_model->update_teachers())
				{
					echo '<script>alert("You Have Successfully Update your account!");</script>';

					redirect(base_url() . 'teachers/teachers_class', 'refresh'); //login content of the login page
				}
				else
				{
					echo '<script>alert("Try Again!");</script>';

					redirect(base_url() . 'teachers/editprofile', 'refresh'); //login content of the edit profile page
				}
			}
		  

		}

	public function update_profile_teacher() // Teacher Update Profile View
		{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher details are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_edit_profile', $data); 
					$this->load->view('teachers/profile');
					$this->load->view('templates/footer');
				}	
			}

			
		}

		public function Update_profile1() //Update Profile for Teacher Update Profile View
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('fname', 'First Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('mname', 'Middle Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('lname', 'Last Name', 'trim|alpha_numeric_spaces|required|max_length[30]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if($this->form_validation->run() == FALSE) 
			{
				$this->session->set_flashdata('error_update', validation_errors());
				redirect(base_url() . 'teachers/update_profile_teacher'); 
			}
			else
			{

				$this->load->model('teachers_model');

				if($query = $this->teachers_model->update_teachers())
				{
					echo '<script>alert("You Have Successfully Update your account!");</script>';

					redirect(base_url() . 'teachers/update_profile_teacher', 'refresh'); 
				}
				else
				{
					echo '<script>alert("Try Again!");</script>';

					redirect(base_url() . 'teachers/update_profile_teacher', 'refresh'); 
				}
			}

		}

		public function Add_subjects() // Add Subjects Teacher MyClasses View
		{

			$this->load->library('form_validation');
			$this->form_validation->set_rules('sem', 'Semester', 'trim|required');
			$this->form_validation->set_rules('sy1', 'School Year', 'trim|numeric|required');
			$this->form_validation->set_rules('sy2', 'School Year', 'trim|numeric|required');
			$this->form_validation->set_rules('room', 'Room', 'trim|alpha_numeric|required');
			$this->form_validation->set_rules('Subject_Code', 'Subject Code', 'trim|alpha_numeric_spaces|required|max_length[15]');
			$this->form_validation->set_rules('Subject_Name', 'Subject_Name', 'trim|alpha_numeric_spaces|required|max_length[60]');
			$this->form_validation->set_rules('Section', 'Section', 'trim|alpha_numeric|required|max_length[10]');
			$this->form_validation->set_rules('abs', 'Allowable Abscences', 'trim|numeric|required|min_length[1]|max_length[15]');

			if($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('error_update', validation_errors());
				redirect(base_url() . 'teachers/teachers_class', 'refresh'); 
			}
			else
			{
				$this->load->model('teachers_model');

				if($query = $this->teachers_model->add_subject())
				{
					echo '<script>alert("You Have Successfully Created a Subject!");</script>';
					redirect(base_url() . 'teachers/teachers_class', 'refresh'); 
				}
				else
				{
					echo '<script>alert("Try Again!");</script>';

					redirect(base_url() . 'teachers/teacher_class', 'refresh');
				}
			}




		}

		public function Teachers_attendance() // Teacher attendance
		{
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-attendance');
					$this->load->view('templates/footer_teacher');
				}	
			}				
		}


	


		
		public function Teachers_attendance_view() // Teacher Attendance view
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-attendance-view');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_attendance_edit() // Teacher Attendance edit
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-attendance-edit');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_attendance_option() // Teacher Class Option
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-attendance-option');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_attendance_add() // Teacher attendance add
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-attendance-add');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Record_attendance() // Record attendance in teacher view
		{
			date_default_timezone_set('Asia/Manila');

			$current_date = date("Y-m-d");
			$time = date("g:i:s A");

			if(isset($_POST['insert']))
			{
		
				$code = $_GET['code'];
				
				$ide = $this->session->userdata("username");

				$result = $this->db->query("SELECT Student_ID, Teacher_FK FROM students, classes WHERE Students_FK=Student_ID AND ClassCode_FK='$code' ORDER BY Last_Name ASC");
					
				$i = 0;
					
				foreach($result->result_array() as $row)
				{
					
				$id = $row['Student_ID'];
				$teacher = $row['Teacher_FK'];

				$Status = $_POST['Status' . $i++];

				
				$add_record = array('Date' => $current_date, 'time' => $time, 'Status' => $Status, 'ClassCode_fk' => $code, 'Students_fk' => $id, 'Teacher_fk' => $teacher, 'Recorded_by' => $ide);

				$subj = $this->db->insert('attendance_record', $add_record);
				

				}
				echo '<script>alert("You Have Successfully Recorded an Attendance!");</script>';
				redirect(base_url() . 'teachers/teachers_attendance?code=' . $code, 'refresh');
				
			}
		}

		public function Record_attendance_sp() // Record attendance in seat plan view
		{
			date_default_timezone_set('Asia/Manila');

			$current_date = date("Y-m-d");
			$time = date("g:i:s A");

			if(isset($_POST['insert']))
			{
		
				$ide = $this->session->userdata("username");
		        $code = $_GET['code'];
		        $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

		        foreach($query->result_array() as $rows)
		        {
		          $acol = $rows['acol'];
		          $arow = $rows['arow'];
		        }
		       
		        $x = 0;
		        
		        for ($row=0; $row < $arow; $row++) 
		        { 
		            for ($col = 0; $col < $acol; $col ++) 
		            	{ 
		              		$x++;

		              		$query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code' AND seat_no='$x'");

		              		$rowz = $query->unbuffered_row('array');

		              		if($x == $rowz['seat_no'])
		              		{
		                		$idn = $rowz['Students_FK'];

		                		$Status = $_POST['Status' . $x];

		                		$add_record = array('Date' => $current_date, 'time' => $time, 'Status' => $Status, 'ClassCode_fk' => $code, 'Students_fk' => $idn, 'Teacher_fk' => $ide, 'Recorded_by' => $ide);

								$subj = $this->db->insert('attendance_record', $add_record);
							
							}

						}

				}

				$code = $_GET['code'];
       			$query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

		        foreach($query->result_array() as $rows)
		        {
		          	$arow = $rows['arow'];
		          	$acol = $rows['acol'];

		          	$bcol = $rows['bcol'];
		          	$brow = $rows['brow'];
		        }

			        $w = $acol*$arow+1;
			        $i = $brow * $bcol + $w;
			        $x = $i;
			        for ($row=0; $row < $brow;$row++) 
		         	{ 
		   				for ($col = 0; $col < $bcol; $col ++) 
		            		{ 
		              			$x--;
		              
				              	$query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code' AND seat_no='$x'");

				              	$rowz = $query->unbuffered_row('array');

				              	if($x == $rowz['seat_no'])
				              	{
					                $idn = $rowz['Students_FK'];

					                $Status = $_POST['Status' . $x];

					                $add_record = array('Date' => $current_date, 'time' => $time, 'Status' => $Status, 'ClassCode_fk' => $code, 'Students_fk' => $idn, 'Teacher_fk' => $ide, 'Recorded_by' => $ide);

									$subj = $this->db->insert('attendance_record', $add_record);

				             	}
		       				 }

		    			}
					
			}

				echo '<script>alert("You Have Successfully Recorded an Attendance!");</script>';
				redirect(base_url() . 'teachers/teachers_attendance?code=' . $code, 'refresh');
		}

		public function Update_attendance() // Update attendance record in teacher view
		{
			if(isset($_POST['update']))
			{
				
				$code = $_GET['code'];
				$date = $_GET['date'];

				$query = $this->db->query("SELECT * from attendance_record where ClassCode_fk = '$code' AND `Date`='$date'");
				$i = 0;
				foreach($query->result_array() as $row)
				{
					$x = $_POST['Status' . $i++];
					$id = $row['Students_fk'];

					$this->db->query("UPDATE attendance_record SET Status='$x' where ClassCode_fk='$code' AND Students_fk='$id' AND `Date`='$date'");

				}
				echo '<script>alert("You Have Successfully Updated Attendance Record!");</script>';
				redirect(base_url() . 'teachers/teachers_attendance_edit?code=' . $code . '&date=' . $date, 'refresh');
			}
				
			
		}

		public function Teachers_generate() // generate pdf file
		{

			$this->load->library('Pdf');
			$this->load->view('teachers/makepdf');
		}

		public function Teachers_list() // Teacher Class List
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-list');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}
		
		public function Teachers_assign() // Teacher Assign Beadle
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-assign');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function assign_b() // Function in teacher assign beadle
		{
			if(isset($_POST['assign']))
				{
					$code = $_GET['code'];

					foreach($_POST['pick'] as $pick)
					{

						$this->load->library('form_validation');
						$this->form_validation->set_rules('pick[]', 'Beadle/Co-Beadle', 'trim|required');
				
						if($this->form_validation->run() == FALSE) //didnt validate
						{
							$this->session->set_flashdata('error_update', validation_errors());
							redirect(base_url() . 'teachers/teachers_assign?code=' . $code, 'refresh');
						}
						else
						{
							$this->load->model('teachers_model');

						
							if($this->teachers_model->Assign_beadle())
							{
									$this->session->set_flashdata('error_update', "<div style='color: red'>Only 1 Beadle and Co-Beadle Allowed! </div>");
									redirect(base_url() . 'teachers/teachers_attendance?code=' . $code, 'refresh');
							}
							else
							{
									redirect(base_url() . 'teachers/teachers_assign?code=' . $code, 'refresh');
							}
							
						}
					}
					

				}
		}

		public function Teachers_daily_form() // Teacher Daily Form
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-daily-form');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Submit_form_daily() // Submitted forms
		{

			if(isset($_POST['insert']))
			{
			$code = $_GET['code']; // ClassCode
			$date = $_GET['date']; // Date
			$id = $this->session->userdata('username'); //Teacher_fk and Recorded by
			$daily = 'daily'; // Form Type
			$appr = 'Approved'; //Status

			$paw = $this->db->query("SELECT * FROM attendance_record WHERE ClassCode_fk='$code' AND `Date`='$date'");

			foreach($paw->result_array() as $p)
			{
				$stat = $p['Status']; // Remark

				if($stat == 'Absent' || $stat == 'Late')
				{
					$id2 = $p['Students_fk']; //Students_fk
					
					$add_form = array('Date' => $date, 'ClassCode' => $code, 'Remark' => $stat, 'Form_Type' => $daily, 'Teacher_fk' => $id, 'Students_fk' => $id2, 'Status' => $appr, 'Recorded_by' => $id);

					$form = $this->db->insert('forms', $add_form);
				}
			}

			echo '<script>alert("You Have Successfully Send form to OSA!");</script>';

			redirect(base_url() . 'teachers/teachers_attendance?code=' . $code, 'refresh');
			
			}
		}

		public function Teachers_daily_form_view() // Teacher Daily Form
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-daily-form-view');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_daily_form_view1() // Teacher Daily Form
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-daily-form-view1');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_approved_forms_daily() // Teacher Approve Forms Daily Monitoring
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-approved-forms-daily');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_approved_forms_warning() // Teacher Approve Forms Warning 
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-approved-forms-warning');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_approved_forms_daily_view() // Teacher Daily Form
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-approved-forms-daily-view');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_approved_forms_warning_view() // Teacher warning Form
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-approved-forms-warning-view');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_pending_forms_daily() // Teacher pending Forms Daily Monitoring
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-pending-forms-daily');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_pending_forms_daily_view() // Teacher pending Forms Daily Monitoring view
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-pending-forms-daily-view');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_issue_forms() // Teacher issue Form
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-issue-forms');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_issue_forms_warning() // Teacher issue Form Warning
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-issue-forms-warning');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_issue_forms_af() // Teacher issue Form AF
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-issue-forms-af');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_issue_forms_af_create() // Teacher issue Form AF
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-issue-forms-af-create');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_issue_forms_warning_view() // Teacher issue Form Warning view
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-issue-forms-warning-view');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_issue_forms_warning_create() // Teacher issue Form Create View
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-issue-forms-warning-create');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Submit_form_warning() // Submitted forms warning
		{	
			date_default_timezone_set('Asia/Manila');

			if(isset($_POST['insert']))
			{
			$code = $_GET['code']; // ClassCode
			$date = date("Y-m-d"); // Date
			$idn = $_GET['id']; // Students_fk
			$id = $this->session->userdata('username'); //Teacher_fk and Recorded by
			$daily = 'warning'; // Form Type
			$appr = 'Approved'; //Status
			$stat = 'Warning'; // Remark
			
					
			$add_form = array('Date' => $date, 'ClassCode' => $code, 'Remark' => $stat, 'Form_Type' => $daily, 'Teacher_fk' => $id, 'Students_fk' => $idn, 'osa_status' => 2, 'Status' => $appr, 'Recorded_by' => $id);

			$form = $this->db->insert('forms', $add_form);

			echo '<script>alert("You Have Successfully Send form to OSA!");</script>';

			redirect(base_url() . 'teachers/teachers_issue_forms_warning?code=' . $code, 'refresh');
			
			}
		}


		public function approved_form_daily() // Submitted forms approved daily
		{	
			date_default_timezone_set('Asia/Manila');

			if(isset($_POST['update']))
			{
			$code = $_GET['code']; // ClassCode
			$date = $_GET['date'];
	
			$id = $this->session->userdata('username'); //Teacher_fk and Recorded by
			$daily = 'daily'; // Form Type
			$appr = 'Approved'; //Status

			$query1 = $this->db->query("SELECT * FROM forms WHERE ClassCode='$code' AND Teacher_fk='$id' AND `Date`='$date' AND Form_Type='$daily'");

           foreach($query1->result_array() as $rows)
            {
            $stat = $rows['Remark'];
            $recorded = $rows['Recorded_by'];
            $idn = $rows['Students_fk'];
			
					
			$add_form = array('Remark' => $stat, 'Form_Type' => $daily, 'osa_status' => 0, 'Status' => $appr);

			$form = $this->db->update('forms', $add_form, array('Students_fk' => $idn, 'ClassCode' => $code, 'Date' => $date));	

			}

			echo '<script>alert("You Have Successfully Approved Daily form!");</script>';

			redirect(base_url() . 'teachers/teachers_attendance?code=' . $code, 'refresh');

			}

			
		}


		public function Submit_form_af() // Submitted forms warning
		{	
			date_default_timezone_set('Asia/Manila');

			if(isset($_POST['insert']))
			{
			$code = $_GET['code']; // ClassCode
			$date = date("Y-m-d"); // Date
			$idn = $_GET['id']; // Students_fk
			$id = $this->session->userdata('username'); //Teacher_fk and Recorded by
			$daily = 'af'; // Form Type
			$appr = 'Approved'; //Status
			$stat = 'AF'; // Remark
			
					
			$add_form = array('Date' => $date, 'ClassCode' => $code, 'Remark' => $stat, 'Form_Type' => $daily, 'Teacher_fk' => $id, 'Students_fk' => $idn, 'osa_status' => 3, 'Status' => $appr, 'Recorded_by' => $id);

			$form = $this->db->insert('forms', $add_form);

			echo '<script>alert("You Have Successfully Send form to OSA!");</script>';

			redirect(base_url() . 'teachers/teachers_issue_forms_af?code=' . $code, 'refresh');
			
			}
		}


		public function Submit_form_daily1() // Submitted forms
		{

			if(isset($_POST['insert']))
			{
			$code = $_GET['code']; // ClassCode
			$date = $_GET['date']; // Date
			$id = $this->session->userdata('username'); //Teacher_fk and Recorded by
			$daily = 'daily'; // Form Type
			$appr = 'Approved'; //Status

			$paw = $this->db->query("SELECT * FROM attendance_record WHERE ClassCode_fk='$code' AND `Date`='$date'");

			foreach($paw->result_array() as $p)
			{
				$stat = $p['Status']; // Remark

				if($stat == 'Absent' || $stat == 'Late')
				{
					$id2 = $p['Students_fk']; //Students_fk
					
					$add_form = array('Date' => $date, 'ClassCode' => $code, 'Remark' => $stat, 'Form_Type' => $daily, 'Teacher_fk' => $id, 'Students_fk' => $id2, 'Status' => $appr, 'Recorded_by' => $id);

					$form = $this->db->insert('forms', $add_form);
				}
			}

			echo '<script>alert("You Have Successfully Send form to OSA!");</script>';

			redirect(base_url() . 'teachers/teachers_attendance?code=' . $code, 'refresh');
			
			}
		}

		public function Teachers_pending_forms_warning() // Teacher pending Forms warning Monitoring
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-pending-forms-warning');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_pending_forms_warning_view() // Teacher pending Forms warning Monitoring view
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-pending-forms-warning-view');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_sp() // Teacher Seat plan view
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				$code = $_GET['code'];

				$class = $this->db->get_where('classes', array('Teacher_FK' => $username, 'ClassCode_FK' => $code))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				elseif(empty($class->arow) && empty($class->acol) && empty($class->brow) && empty($name->bcol))
				{
					redirect(base_url('teachers/teachers_assignseat?code=' . $code));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-sp');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}


		public function configure_sp()
		{
			$a_col = $_POST['lcol'];
			$a_row= $_POST['lrow'];
			$b_col = $_POST['rcol'];
			$b_row = $_POST['rrow'];
			$code = $_GET['code'];

			$query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

			foreach($query->result_array() as $rows)
			{
				$sid = $rows['Students_FK'];
				$code1 = $rows['ClassCode_FK'];
				$tk = $rows['Teacher_FK'];


				$add_sp = array('arow' => $a_row, 'acol' => $a_col, 'brow' => $b_row, 'bcol' => $b_col);

				$this->db->update('classes', $add_sp, array('ClassCode_FK' => $code1, 'Students_FK' => $sid, 'Teacher_FK' => $tk));

			}

			

			redirect(base_url() . 'teachers/teachers_assignseat?code=' . $code, 'refresh');
		}

		public function Teachers_assignseat() // Teacher Seat plan view assign seat
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();


				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-assignseat');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function assign_seat() // Function inside seat plan assigning seat
		{
			if(isset($_POST['save']))
			{
				$code = $_GET['code'];
				$name = $_POST['name'];
				$x = $_POST['id2'];

				$this->db->query("UPDATE classes set seat_no = '$x' where Students_FK = '$name' AND ClassCode_FK='$code'");

				echo '<script>alert("You Have Successfully Assign a seat!");</script>';

				redirect(base_url() . 'teachers/teachers_assignseat?code=' . $code, 'refresh');

			}
		
		}

		public function Teachers_sp_attendance() // Teacher Seat plan attendance
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();


				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-sp-attendance');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Record_attendance_seat()
		{
			date_default_timezone_set('Asia/Manila');
			$current_date = date("Y-m-d");
			$time = date("g:i:s A");
			$status = $_POST['status'];
			$code = $_GET['code'];
			$id = $this->session->userdata('username');
			$ids = $_GET['sid'];

			$add_record = array('Date' => $current_date, 'time' => $time, 'Status' => $status, 'ClassCode_fk' => $code, 'Students_fk' => $ids, 'Teacher_fk' => $id, 'Recorded_by' => $id);

			$subj = $this->db->insert('attendance_record', $add_record);

			
			redirect(base_url() . 'teachers/teachers_attendance?code=' . $code, 'refresh');

		}

		public function Teachers_pending_forms_af() // Teacher pending Forms af Monitoring
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-pending-forms-af');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}

		public function Teachers_pending_forms_af_view() // Teacher pending Forms af Monitoring view
		{
			
			if(!isset($_SESSION['username']))
			{
				redirect(base_url());
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 0)
			{
				redirect(base_url('students/students_class'));
			}
			elseif(isset($_SESSION['username']) && $_SESSION['role'] == 2)
			{
				redirect(base_url('osa/osa_class'));
			}
			else
			{
				$username = $this->session->userdata('username');

				$name = $this->db->get_where('teacher', array('Faculty_ID' => $username))->row();

				//Check if Teacher personal information are empty
				if(empty($name->First_Name) && empty($name->Last_Name) && empty($name->Middle_Name) && empty($name->Course))
				{
					redirect(base_url('teachers/editprofile'));
				}
				else
				{
					$try = $this->session->userdata('username');

					$name = $this->db->get_where('teacher', array('Faculty_ID' => $try))->row();

					$data['username'] = $name->First_Name;

					$this->load->view('templates/header_teacher', $data);
					$this->load->view('teachers/teacher-pending-forms-af-view');
					$this->load->view('templates/footer_teacher');
				}	
			}		
		}


		public function approved_form_warning() // Submitted forms approved daily
		{	
			date_default_timezone_set('Asia/Manila');

			if(isset($_POST['update']))
			{
			$code = $_GET['code']; // ClassCode
			$date = $_GET['date'];
			$idk = $_GET['id'];
			$id = $this->session->userdata('username'); //Teacher_fk and Recorded by
			$daily = 'warning'; // Form Type
			$appr = 'Approved'; //Status

			$query1 = $this->db->query("SELECT * FROM forms WHERE ClassCode='$code' AND Teacher_fk='$id' AND `Date`='$date' AND Form_Type='$daily'");

           foreach($query1->result_array() as $rows)
            {
            $stat = 'Warning';
            $recorded = $rows['Recorded_by'];
         
			
					
			$add_form = array('Remark' => $stat, 'Form_Type' => $daily, 'osa_status' => 3, 'Status' => $appr, 'Recorded_by' => $recorded);

			$form = $this->db->update('forms', $add_form, array('Students_fk' => $idk, 'ClassCode' => $code, 'Date' => $date));	

			}

			echo '<script>alert("You Have Successfully Approved Warning form!");</script>';

			redirect(base_url() . 'teachers/teachers_pending_forms_warning?code=' . $code, 'refresh');

			}

			
		}

	}
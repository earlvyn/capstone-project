<?php

class Teachers_model extends CI_Model
{
	public function __construct()
	{
			parent::__construct();
			$this->load->helper('url');
			$this->load->helper('form');
	}

	public function update_teachers()
	{
		$fname = $this->input->post('fname');
		$mname = $this->input->post('mname');
		$lname = $this->input->post('lname');

		$username = $this->session->userdata('username');
		
		$update_t = array('First_Name' => $fname, 'Middle_Name' => $mname, 'Last_Name' => $lname);

		$update_ts = $this->db->update('teacher', $update_t, array('Faculty_ID' => $username));

		return $update_ts;
	}

	public function update_teachers2()
	{
		$fname = $this->input->post('fname');
		$mname = $this->input->post('mname');
		$lname = $this->input->post('lname');
		$password = $this->input->post('password');

		$username = $this->session->userdata('username');
		
		$update_t = array('First_Name' => $fname, 'Middle_Name' => $mname, 'Last_Name' => $lname);
		$update_u = array('password' => $password);

		$update_ts = $this->db->update('teacher', $update_t, array('Faculty_ID' => $username));
		$update_us = $this->db->update('users', $update_u, array('username' => $username));

		return $update_ts;
		return $update_us;
	}


	public function Add_subject()
	{
		//Generate Random String
		function generateRandomString($length = 6) 
		{
		    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		   		$charactersLength = strlen($characters);
		    	$randomString = '';

		   		for ($i = 0; $i < $length; $i++) 
		   		{
		       		$randomString .= $characters[rand(0, $charactersLength - 1)];
				}

		return $randomString;
		}
		
		$ClassCode = generateRandomString();

		$subject_code = $this->input->post('Subject_Code');
		$subject_name = $this->input->post('Subject_Name');
		$section = $this->input->post('Section');
		$code = generateRandomString();
		$Teacher_FK = $this->session->userdata('username');
		$max_a = $this->input->post('abs');
		$sem = $this->input->post('sem');
		$sy1 = $this->input->post('sy1');
		$sy2 = $this->input->post('sy2');
		$syear = $sy1 . '-' . $sy2;
		$room = $this->input->post('room');

		$add_s = array('Subject_Code' => $subject_code, 'Subject_Name' => $subject_name, 'Section' => $section, 'ClassCode' => $code, 'Teacher_FK' => $Teacher_FK, 'max_absent' => $max_a, 'semester' => $sem, 'syear' => $syear, 'room' => $room);

		$subj = $this->db->insert('subjects', $add_s);

		return $subj;	

	}

	public function Assign_beadle()
	{
		$code = $_GET['code'];

		$result = $this->db->query("SELECT Student_ID FROM students, classes WHERE Students_FK=Student_ID AND ClassCode_FK='$code' ORDER BY Last_Name ASC");
			
		
		foreach($_POST['pick'] as $pick)
		{
			$row = $result->unbuffered_row('array');

			$data = $row['Student_ID'];

			if($pick == 'Beadle')
			{
				$this->db->limit(1);
				$this->db->query("UPDATE classes set role = 2 where Students_FK = '$data' AND ClassCode_FK='$code'");
				$this->db->limit(1);
			  		    	
			}
			elseif($pick == 'Co-Beadle')
			{
				$this->db->limit(1);
				$this->db->query("UPDATE classes set role = 1 where Students_FK = '$data' AND ClassCode_FK='$code'");
				$this->db->limit(1);

			}
			elseif($pick == 'None')
			{
				$this->db->query("UPDATE classes set role = 0 where Students_FK = '$data' AND ClassCode_FK='$code'");
				    	
			}

		}
	}

	public function config_seatplan()
	{

		$a_col = $_POST['lcol'];
		$a_row= $_POST['lrow'];
		$b_col = $_POST['rcol'];
		$b_row = $_POST['rrow'];

		$code = $_GET['code'];

		$query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

		foreach($query->result_array() as $rows)
		{
			$sid = $rows['Students_FK'];
			$code1 = $rows['ClassCode_FK'];
			$tk = $rows['Teacher_FK'];


			$add_sp = array('arow' => $a_row, 'acol' => $a_col, 'brow' => $b_row, 'bcol' => $b_col);

			$try = $this->db->update('classes', $add_sp, array('ClassCode_FK' => $code1, 'Students_FK' => $sid, 'Teacher_FK' => $tk));
		}

		return $try;
	}

	

}
<?php

class Students_model extends CI_Model
{
	public function __construct()
	{
			parent::__construct();
			$this->load->helper('url');
			$this->load->helper('form');
	}

	public function update_students()
	{

		$fname = $this->input->post('fname');
		$mname = $this->input->post('mname');
		$lname = $this->input->post('lname');
		$year = $this->input->post('year');
		$course = $this->input->post('course');

		$username = $this->session->userdata('username');
		
		$update_s = array('First_Name' => $fname, 'Middle_Name' => $mname, 'Last_Name' => $lname, 'Year' => $year, 'Course' => $course);
	
		$update_st = $this->db->update('students', $update_s, array('Student_ID' => $username));
	
		return $update_st;
		
	}

	public function update_students2()
	{

		$fname = $this->input->post('fname');
		$mname = $this->input->post('mname');
		$lname = $this->input->post('lname');
		$year = $this->input->post('year');
		$course = $this->input->post('course');
		$password = $this->input->post('password');

		$username = $this->session->userdata('username');
		
		$update_s = array('First_Name' => $fname, 'Middle_Name' => $mname, 'Last_Name' => $lname, 'Year' => $year, 'Course' => $course);
		$update_u = array('password' => $password);

		$update_st = $this->db->update('students', $update_s, array('Student_ID' => $username));
		$update_ut = $this->db->update('users', $update_u, array('username' => $username));

		return $update_st;
		return $update_ut;
		
	}

	public function Enroll_class()
	{
		if(isset($_POST['enroll']))
		{
		$code = $this->input->post('code1');
		$user = $this->session->userdata('username');

		$result = $this->db->query("SELECT ClassCode, Teacher_FK FROM subjects WHERE ClassCode='$code'");

		$query = $this->db->query("SELECT ClassCode_FK, Students_FK from classes where ClassCode_FK='$code' AND Students_FK='$user'");

		if($query->num_rows() == 1)
		{
			return false;
		}
		else
		{
			foreach($result->result_array() as $row)
			{
				$try = $row['ClassCode'];
				$teacher = $row['Teacher_FK'];
			}

			$enroll = array('ClassCode_FK' => $try, 'Students_FK' => $user, 'role' => '0', 'Teacher_FK' => $teacher);

			$subj = $this->db->insert('classes', $enroll);

			return $subj;
			
		}
		}
		
	}

	public function Validate_class($code1)
	{
		$this->db->where('ClassCode', $code1);

		$query = $this->db->get('subjects');

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	

}
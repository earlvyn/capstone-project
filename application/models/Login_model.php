<?php

class Login_model extends CI_Model
{
	public function __construct()
	{
			parent::__construct();
			$this->load->helper('url');
			$this->load->helper('form');
			
	}
	
	public function validate_login($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);

		$query = $this->db->get('users');

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function validate_register()
	{
		$id_num = $this->input->post('idnumber');
		if($id_num[0] == 's' || $id_num[0] == 'S') // Check if its a student or teacher
		{
			//Data inserted into users table
			$new_user_student = array(
			'username' => $this->input->post('idnumber'),
			'password' => $this->input->post('passwordsignup'), //use md5() for hasing password data
			'role' => '0');

			//$id_s = substr($this->input->post('idnumber'),1,10); //insert idnumber only

			//Data inserted into students table
			$id_data_s = array(
				'Student_ID' => $this->input->post('idnumber'), 
				'Email' => $this->input->post('emailsignup')
			);

			$insert_s = $this->db->insert('users', $new_user_student);
			$insert_s1 = $this->db->insert('students', $id_data_s);

			return $insert_s;
			return $insert_s1;
		}
		else
		{
			//Data inserted into users table
			$new_user_teacher = array(
			'username' => $this->input->post('idnumber'),
			'password' => $this->input->post('passwordsignup'), //use md5() for hasing password data
			'role' => '1');

			//$id_t = substr($this->input->post('idnumber'),1,10); //insert idnumber only

			//Data inserted into teacher table
			$id_data_t = array(
				'Faculty_ID' => $this->input->post('idnumber'), 
				'Email' => $this->input->post('emailsignup')
			);

			$insert_t = $this->db->insert('users', $new_user_teacher); //users
			$insert_t1 = $this->db->insert('teacher', $id_data_t); // teacher

			return $insert_t;
			return $insert_t1;
		}

		
	}

}
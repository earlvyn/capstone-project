 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

          <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('students/students_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('beadle/beadle_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
            <li class="breadcrumb-item active">Select View</li>
          </ol>



          <div class="table-responsive">
            <a href="<?php echo base_url('beadle/beadle_attendance_add?code=' . $code); ?>" class="btn btn-success" style="float: none; margin-left: 60px;"><!--top right bottom left-->
              <div class="inside_left"><h1>List View</h1>
                <h1><i class="fas fa-list-ul" style="font-size: 200px;"></i></h1>
              </div>
            </a>
            <a href="<?php echo base_url('beadle/beadle_sp_attendance?code=' . $code); ?>" class="btn btn-info" style="float: none; margin-left: 40px;">
              <div class="inside_right">
                <h1>Seat Plan View</h1>
                <h1><i class="fab fa-buromobelexperte" style="font-size: 200px;"></></i></h1>
              </div>
            </a>
          </div>
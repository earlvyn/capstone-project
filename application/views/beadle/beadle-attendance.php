 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

          <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('students/students_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">Attendance - <?php echo $s_code; ?></li>
          </ol>

           <nav class="navbar-expand">

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <li class="nav-item dropdown no-arrow">
          <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <button class="btn btn-info"><i class="fas fa-grip-vertical"></i> Menu</button>
          </a>
          <div class="dropdown-menu dropdown-menu-left" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?php echo base_url('beadle/beadle_config?code=' . $code); ?>"><i class="fas fa-cog"></i> Seat Plan Config</a>
            <a class="dropdown-item" href="<?php echo base_url('beadle/beadle_list?code=' . $code); ?>" class="btn btn-info"><i class="fas fa-th-list"></i> Classlist</a>
            <a class="dropdown-item" href="<?php echo base_url('beadle/beadle_sp?code=' . $code); ?>"><i class="fas fa-swatchbook"></i> Seat Plan</a>
          </div>
        </li>
        &nbsp;
        <li class="nav-item dropdown no-arrow">
          <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <button class="btn btn-info"><i class="fas fa-book"></i> Forms</button>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
             <a class="dropdown-item" href="<?php echo base_url('beadle/beadle_issue_forms?code=' . $code); ?>" class="btn btn-info"><i class="fas fa-exclamation-circle"></i> Issue Warning/AF</a>
             <a class="dropdown-item" href="<?php echo base_url('teachers/teachers_pending_forms_daily?code=' . $code); ?>"><i class="fab fa-wpforms"></i> Pending Forms</a>
            <a class="dropdown-item" href="<?php echo base_url('teachers/teachers_approved_forms_daily?code=' . $code); ?>" class="btn btn-info"><i class="far fa-file-alt"></i> Approved Forms</a>
            
          </div>
        </li>

      </ul>

    </nav>

     <br />
          <!-- DataTables Example -->
          <div class="card mb-3" >
            <div class="card-header" >
              <i class="fas fa-table" ></i>
              Class Attendance</div>
            <div class="card-body" >
              <div class="table-responsive">
                 <form action="<?php echo base_url('beadle/beadle_generate?code=' . $code); ?>" method="POST">
                  <a href="<?php echo base_url('beadle/beadle_attendance_option?code=' . $code); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add Record</a>
                  <?php 
                  $code = $_GET['code'];
                  $sql = $this->db->query("SELECT distinct `Date` FROM attendance_record WHERE ClassCode_fk='$code'");
                  if($sql->num_rows() > 0) {
                  ?>
                  <button type="submit" name="create_pdf" class="btn btn-success" style="float: right;">    
                  <i class="fas fa-print"></i> Generate Summary
                  </button>
                  <br /> <br />
                  <?php } ?>
                  </form>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead class="table-heading">
                    <tr align="center">
                      <th>#</th>
                      <th>Date</th>
                      <th>Description</th>
                      <th>Action</th>
                      <th>Issue Daily Form</th>
                      <th>Approval</th>       
                    </tr>
                  </thead>
                  <tbody class="table-body" align="center">
                  <?php
      
                    $code = $_GET['code'];
                    $i = 1;

                    $query = $this->db->query("SELECT distinct `Date`, ClassCode_fk from attendance_record where ClassCode_fk='$code'");


                    if($query->num_rows() > 0)
                    {
                      $i = 0;

                    foreach($query->result_array() as $rows)
                    { 
                      $i++;
                      $date = $rows['Date'];

                        $curr = date("F j, Y (l)", strtotime($date));
                  ?>
                
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $curr; ?> </td>
                      <td>Regular Class Session</td>
                      <td><a href="<?php echo base_url('beadle/beadle_attendance_view?code=' . $code . '&date=' . $rows['Date']) ?>"><i class="fas fa-eye" style="padding-right: 25px; color: black;"> View</i></a><a href="<?php echo base_url('beadle/beadle_attendance_edit?code=' . $code . '&date=' . $rows['Date']) ?>"><i class="fas fa-cog" style="color: black;"> Edit</i></a></td>
                       <?php
                       $sql = $this->db->query("SELECT distinct `Date`, Status from forms Where ClassCode='$code' And `Date`='$date'");

                      if($sql->num_rows() == 0)
                      { 
                      ?>
        
                      <td align="center"><a href="<?php echo base_url('beadle/beadle_daily_form?code=' . $code . '&date=' . $rows['Date']) ?>""><i class="fas fa-share-square"> Send Form</i></a></td>
                      <td>None</td>
                      <?php
                      }
                      else
                      { 
                      ?>
                        <td align="center"><a href="<?php echo base_url('beadle/beadle_daily_form_view?code=' . $code . '&date=' . $rows['Date']) ?>""><i class="fas fa-eye" style="color: black;"> Preview</i></a></td>
                        <?php 

                        foreach($sql->result_array() as $rows)
                        {
                          if($rows['Status'] == 'Pending') {
                        ?>
                        <td><i class="fas fa-clock" style="color: black;"> Pending</i></td>
                        <?php 
                        }
                        elseif($rows['Status'] == 'Approved')
                        {
                        ?>
                          <td><i class="fas fa-check" style="color: green;"> Approved</i></td>
                      <?php }}} ?> 
                    </tr>
                    <?php } }?>
                  </tbody>
                </table>
              </div>
            </div>
            <!--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
          </div>

        </div>

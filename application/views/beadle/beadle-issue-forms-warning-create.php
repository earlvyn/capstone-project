<div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

          <?php
        
          $code = $_GET['code'];
          $id = $_GET['id'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
            $max = $row['max_absent'];
            $tk = $row['Teacher_FK'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('students/students_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('beadle/beadle_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('beadle/beadle_issue_forms?code=' . $code); ?>">Issue Forms</a>
            </li>
            <li class="breadcrumb-item active">Warning</li>
          </ol>

           <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Notie of Warning On Attendance</div>
            <div class="card-body">
              <div class="table-responsive">
                 <form action="<?php echo base_url('beadle/submit_form_warning?code=' . $code . '&id=' . $id); ?>" method="POST">
                <table border="1px solid black" align="center" width="60%">
                    <thead class="table-heading">
                    <tr>
                    <th colspan="2" style="text-align: center; height: 70px;">Notie of Warning On Attendance</th>
                    </tr>
                    </thead>
                    <tbody class="table-body">
                    <?php
                      date_default_timezone_set('Asia/Manila');
                      $code = $_GET['code'];
                      $id = $_GET['id'];
                      $date = date("Y-m-d");
                    
                       //$date = date("F j, Y (l)");
                       $curr = date("F j, Y (l)", strtotime($date));
                      
                      $result = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

                      $sql = $this->db->query("SELECT * from students WHERE Student_ID='$id'");

                      $rec = $sql->row();

                      $fname = $rec->First_Name;
                      $mname = substr($rec->Middle_Name,0,1);
                      $lname = $rec->Last_Name;

                      $name = $fname . ' ' . $mname . '. ' . $lname;

                      $row = $result->row();

                      $curr = date("F j, Y (l)", strtotime($date));
                      ?>
                      <tr>
                        <td style="height: 40px;">&nbsp;<b>Name: </b><?php echo $name; ?></td>
                        <td style="height: 40px;">&nbsp;<b>Subject Code: </b><?php echo $row->Subject_Code;?></td>
                      </tr>
                       <tr>
                         <td colspan="0" style="height: 50px;">&nbsp;<b>Date: </b><?php echo $curr; ?></td>
                         <td style="height: 40px;">&nbsp;<b>Section: </b><?php echo $row->Section; ?></td>
                      </tr>
                    </tbody>
                </table>
                <table border="1px solid black" align="center" width="60%">
                  <tr>
                  <td class="table-body" colspan="3">Please be informed that you have incurred <b style="font-size: 20px; color: red;"><u><?php echo $_GET['num']; ?></u></b> No. Of Absences in this Subject. May this serve as a <b style="color : red;">WARNING</b> that should you accumulate more than the allowable absences, you will receive a NOTICE OF FAILURE DUE TO ABSENCES (AF). <br /><br />

                  You are advised to consult/verify this matter iwth your subject teacher. <br /><br />

                  Thank you very much! <br />
                  </td>
                  </tr> 
                </table>
                <table border="1px solid black" align="center" width="60%">
                <tr>
                <th class="table-body" style="text-align: center; height: 60px;"><input type ="submit" name="insert" value="Send to Teacher" class="btn btn-success"/></th>
                </tr>
                </table>
              </form>
              </div>
            </div>
          </div>

        </div>

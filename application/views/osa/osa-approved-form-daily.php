<div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

        <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('osa/osa_class'); ?>">My Forms</a>
            </li>
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('osa/osa_form_daily'); ?>"> Daily Attendance Monitoring Form</a>
            </li>
            <li class="breadcrumb-item active">Approved Forms</li>
          </ol>

      <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
             Approved Daily Attendance Monitoring Form</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" align="center">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>Subject Code</th>
                      <th>Subject Name</th>
                      <th>Section</th>
                      <th>Date</th>
                      <th>Instructor</th>
                      <th>Record</th>       
                    </tr>
                  </thead>
                 <tbody class="table-body" align="center">
                    <?php 

                    $sql = $this->db->query("SELECT distinct `Date`, ClassCode FROM forms WHERE osa_status='1'");

                    $i = 1;

                    foreach($sql->result_array() as $rows)
                    {

                      $date = $rows['Date'];
                      $code = $rows['ClassCode'];

                      $curr = date("F j, Y (l)", strtotime($date));

                      $result = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

                      $q = $result->row();

                      $id = $q->Teacher_FK;

                      $query = $this->db->query("SELECT * FROM teacher WHERE Faculty_ID='$id'");

                      $w = $query->row();

                      $fname = $w->First_Name;
                      $mname = substr($w->Middle_Name,0,1);
                      $lname = $w->Last_Name;

                      $name = '<b>' . $lname . '</b>, ' . $fname . ' ' . $mname . '.';

                    ?>
                    <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $q->Subject_Code; ?></td>
                    <td><?php echo $q->Subject_Name; ?></td>
                    <td><?php echo $q->Section; ?></td>
                    <td><?php echo $curr; ?></td>
                    <td><?php echo $name; ?></td>
                    <td><a href="<?php echo base_url('osa/osa_view_daily?code=' . $code . '&date=' . $date); ?>"><i class="fas fa-eye"></i></td>
                    </tr>
                    <?php $i++; }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
<div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

           <?php 

          $code = $_GET['code'];
          $id = $_GET['id'];
          $date = $_GET['date'];
          ?>

        <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('osa/osa_class'); ?>">My Forms</a>
            </li>
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('osa/osa_form_warning'); ?>">Notice of Warning On Attendance</a>
            </li>
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('osa/osa_pending_form_warning'); ?>">Pending Forms</a>
            </li>
            <li class="breadcrumb-item active">View Record</li>
          </ol>


        
         <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Notice of Warning on Attendance</div>
            <div class="card-body">
              <div class="table-responsive">
                 <form action="<?php echo base_url('osa/submit_form_warning?code=' . $code . '&id=' . $id . '&date=' . $date); ?>" method="POST">
                <table border="1px solid black" align="center" width="60%">
                    <thead class="table-heading">
                    <tr>
                    <th colspan="2" style="text-align: center; height: 70px;">Notie of Warning On Attendance</th>
                    </tr>
                    </thead>
                    <tbody class="table-body">
                    <?php
                     
                      $code = $_GET['code'];
                      $id = $_GET['id'];
                      $date = $_GET['date'];
                    
                      $sql1 = $this->db->query("SELECT Teacher_fk from forms WHERE ClassCode='$code' AND Students_fk='$id' AND Form_Type='warning'"); 

                      $waw = $sql1->row();

                      $tk = $waw->Teacher_fk;
                      
                      $result = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

                      $sql = $this->db->query("SELECT * from students WHERE Student_ID='$id'");

                      $rec = $sql->row();

                      $fname = $rec->First_Name;
                      $mname = substr($rec->Middle_Name,0,1);
                      $lname = $rec->Last_Name;

                      $name = $fname . ' ' . $mname . '. ' . $lname;

                      $row = $result->row();

                      $curr = date("F j, Y (l)", strtotime($date));
                      ?>
                      <tr>
                        <td style="height: 40px;">&nbsp;<b>Name: </b><?php echo $name; ?></td>
                        <td style="height: 40px;">&nbsp;<b>Subject Code: </b><?php echo $row->Subject_Code;?></td>
                      </tr>
                       <tr>
                         <td colspan="0" style="height: 50px;">&nbsp;<b>Date: </b><?php echo $curr; ?></td>
                         <td style="height: 40px;">&nbsp;<b>Section: </b><?php echo $row->Section; ?></td>
                      </tr>
                    </tbody>
                </table>
                <table border="1px solid black" align="center" width="60%">
                  <tr>
                  <td class="table-body" colspan="3">Please be informed that you have incurred <b style="font-size: 20px; color: red;"><u><?php echo $_GET['num']; ?></u></b> No. Of Absences in this Subject. May this serve as a <b style="color : red;">WARNING</b> that should you accumulate more than the allowable absences, you will receive a NOTICE OF FAILURE DUE TO ABSENCES (AF). <br /><br />

                  You are advised to consult/verify this matter iwth your subject teacher. <br /><br />

                  Thank you very much! <br />
                  </td>
                  </tr> 
                </table>
                <table border="1px solid black" align="center" width="60%">
                <tr>
                <th class="table-body" style="text-align: center; height: 60px;"><input type ="submit" name="insert" value="Receive Form" class="btn btn-success"/></th>
                </tr>
                </table>
              </form>
              </div>
            </div>
          </div>

        </div>
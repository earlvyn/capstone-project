<div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

        <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('osa/osa_class'); ?>">My Forms</a>
            </li>
            <li class="breadcrumb-item active">Notice of Warning On Attendance</li>
          </ol>


             <div class="table-responsive">
            <a href="<?php echo base_url('osa/osa_pending_form_warning'); ?>" class="btn btn-success" style="float: none; margin-left: 60px;"><!--top right bottom left-->
              <div class="inside_left"><h1>Pending Forms</h1>
                <h1><i class="fab fa-wpforms" style="font-size: 200px;"></i></h1>
              </div>
            </a>
            <a href="<?php echo base_url('osa/osa_approved_form_warning'); ?>" class="btn btn-info" style="float: none; margin-left: 40px;">
              <div class="inside_right">
                <h1>Approved Forms</h1>
                <h1><i class="far fa-file-alt" style="font-size: 200px;"></></i></h1>
              </div>
            </a>
          </div>

        </div>
 <div id="content-wrapper" style="background-color: #dfe3ee;">
        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb" >
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('osa/osa_class'); ?>">My Classes</a>
            </li>

          </ol>
          
    <div class="row">
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-book-open"></i>
                  </div>
                  <div class="mr-5">Daily Attendance Monitoring Forms</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('osa/osa_form_daily'); ?>">
                  <span class="float-left">View Record</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-book-open"></i>
                  </div>
                  <div class="mr-5">Notice of Warning On Attendance</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('osa/osa_form_warning'); ?>">
                  <span class="float-left">View Record</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-book-open"></i>
                  </div>
                  <div class="mr-5">Notice of AF On Attendance</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('osa/osa_form_af'); ?>">
                  <span class="float-left">View Record</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>

            
          
           
     </div>

  </div>
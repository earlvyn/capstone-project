<div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

        <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('osa/osa_class'); ?>">My Forms</a>
            </li>
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('osa/osa_form_warning'); ?>">Notice of Warning On Attendance</a>
            </li>
            <li class="breadcrumb-item active">Pending Forms</li>
          </ol>

       <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
             Pending Warning Form</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" align="center">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>Subject Code</th>
                      <th>Subject Name</th>
                      <th>Section</th>
                      <th>Name</th>
                      <th>Instructor</th>
                      <th>Record</th>       
                    </tr>
                  </thead>
                 <tbody class="table-body" align="center">
                   <?php 

                   $formtype = 'warning';
                   $stat = '2';

                   $sql = $this->db->query("SELECT * from forms WHERE Form_Type='$formtype' AND osa_status='$stat'");
                   $i = 1;
                   foreach($sql->result_array() as $rows)
                   {
                      $code = $rows['ClassCode'];
                      $id = $rows['Students_fk'];
                      $tk = $rows['Teacher_fk'];
                      $date = $rows['Date'];

                      $result = $this->db->query("SELECT * from subjects WHERE ClassCode='$code'");

                      $q = $result->row();

                     $query = $this->db->query("SELECT * from students WHERE Student_ID='$id'");

                      $rec = $query->row_array();

                      $query1 = $this->db->query("SELECT * from teacher WHERE Faculty_ID='$tk'");

                      $w = $query1->row_array();

                      $sql1 = $this->db->query("SELECT * from forms WHERE Form_Type='daily' AND osa_status='1' AND ClassCode='$code' AND Teacher_fk='$tk'");


                      $fname = $rec['First_Name'];
                      $mname = substr($rec['Middle_Name'],0,1);
                      $lname = $rec['Last_Name'];

                      $name = $fname . ' ' . $mname . '.' . $lname;

                      $fname1 = $w['First_Name'];
                      $lname1 = $w['Last_Name'];

                      $name1 = $fname1 . ' ' . $lname1;
                      $w = 0;

                      foreach($sql1->result_array() as $row)
                      {
                      if($row['Students_fk'] == $id)
                      {
                        if($row['Remark'] == 'Absent')
                        {
                          $w++;
                        }
                      }
                      }

                   ?>
                   <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $q->Subject_Code; ?></td>
                    <td><?php echo $q->Subject_Name; ?></td>
                    <td><?php echo $q->Section; ?></td>
                    <td><?php echo $name; ?></td>
                    <td><?php echo $name1; ?></td>
                    <td align="center"><a href="<?php echo base_url('osa/osa_accept_warning?code=' . $code . '&id=' . $id . '&num=' . $w . '&date=' . $date); ?>"><i class="fas fa-eye"> View</i></a></td>
                  </tr>
                  <?php $i++; }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
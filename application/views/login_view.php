<div id="container_demo" >
		<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
				<a class="hiddenanchor" id="toregister"></a>
				<a class="hiddenanchor" id="tologin"></a>
		<div id="wrapper">
			<div id="login" class="animate form">
				 <form action="<?php echo base_url(); ?>login/submit_loginuser" method="POST">
				 <img src="<?php echo base_url('images/seal.png'); ?>" class="seal">
								<br />
								<br />
								<br />
								<br />
								<br />
                                <h1>Welcome to AAMS</h1> 
							<p> 
								<label for="usernameq" class="uname" data-icon="u" > Username</label>
								<input id="username" name="username" required="required" maxlength="10" type="text" placeholder="ID Number"/>
							</p>
							<p> 
								<label for="password" class="youpasswd" data-icon="p"> Password </label>
								<input id="password" name="password" required="required" type="password" placeholder="Password" /> 
							</p>
							<?php echo $this->session->flashdata('error_login'); ?>
							<p class="login button"> 
								<input type="submit" value="Login" /> 
							</p>

							<p class="change_link">
								Not a member yet ?
								<a href="#toregister" class="to_register">REGISTER</a>
							</p>	
				</form>
			</div>
		
				 <div id="register" class="animate form">
                            <!--<form action="<?php //echo base_url(); ?>login/register" method="POST">-->                      
                            	<?php echo form_open('login/register'); ?>
                                <h1> Sign up </h1> 
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="u">ID-Number(s for students and p for teachers)</label>
                                    <input id="idnumber" name="idnumber" type="text" maxlength="10" value="<?php echo $this->session->flashdata('idnumber'); ?>" required="required" placeholder="ex. s201310183 or p201310183"/>	
                                </p>
                                <p> 
                                    <label for="emailsign" class="youmail" data-icon="e" >Gbox Email</label>
                                    <input id="emailsignup" name="emailsignup" type="email" required="required" value="<?php echo $this->session->flashdata('emailsignup'); ?>" placeholder="jrizal@gbox.adnu.edu.ph" /> 
                                </p>
                                <p> 
                                    <label for="passwordsign" class="youpasswd" data-icon="p">Password </label>
                                    <input id="passwordsignup" name="passwordsignup" type="password" required="required" placeholder="eg. asdqwe123" />
                                </p>
                                <p> 
                                    <label for="passwordsign_confirm" class="youpasswd" data-icon="p">Please confirm your password </label>
                                   <input id="passwordsignup_confirm" name="passwordsignup_confirm" required="required" type="password" placeholder="eg. asdqwe123"/>
                                </p>
              					<?php echo $this->session->flashdata('error_signup'); ?>
                                <p class="signin button"> 
									<!--<input type="submit" value="Sign up"/>-->
									<?php echo form_submit('submit', 'Sign Up'); ?> 
								</p>
                                <p class="change_link">  
									Already a member ?
									<a href="#tologin" class="to_register"> Go and log in </a>
								</p>
                            <?php echo form_close(); ?>
                </div>
			</div>
	</div>
 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

           <?php
        
          $code = $_GET['code'];
         date_default_timezone_set('Asia/Manila');

          $date = date("Y-m-d");
          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");
          $id = $_GET['id'];
          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
            $max = $row['max_absent'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_pending_forms_warning?code=' . $code); ?>">Pending Forms</a>
            </li>
             </li>
            <li class="breadcrumb-item active">Warning Pending Form View</li>
          </ol>

        
         <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Notice of Warning on Attendance</div>
            <div class="card-body">
              <div class="table-responsive">
                  <form action="<?php echo base_url('teachers/approved_form_warning?code=' . $code . '&date=' . $date . '&id=' . $id); ?>" method="POST">
                <table border="1px solid black" align="center" width="60%">
                    <thead class="table-heading">
                    <tr>
                    <th colspan="2" style="text-align: center; height: 70px;">Notie of Warning On Attendance</th>
                    </tr>
                    </thead>
                    <tbody class="table-body">
                    <?php
                     
                      $code = $_GET['code'];
                      $id = $_GET['id'];
                    
                      $sql1 = $this->db->query("SELECT `Date` from forms WHERE ClassCode='$code' AND Students_fk='$id' AND Form_Type='warning'"); 

                      $waw = $sql1->row();

                      $date = $waw->Date;
                      
                      $result = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

                      $sql = $this->db->query("SELECT * from students WHERE Student_ID='$id'");

                      $rec = $sql->row();

                      $fname = $rec->First_Name;
                      $mname = substr($rec->Middle_Name,0,1);
                      $lname = $rec->Last_Name;

                      $name = $fname . ' ' . $mname . '. ' . $lname;

                      $row = $result->row();

                      $curr = date("F j, Y (l)", strtotime($date));
                      ?>
                      <tr>
                        <td style="height: 40px;">&nbsp;<b>Name: </b><?php echo $name; ?></td>
                        <td style="height: 40px;">&nbsp;<b>Subject Code: </b><?php echo $row->Subject_Code;?></td>
                      </tr>
                       <tr>
                         <td colspan="0" style="height: 50px;">&nbsp;<b>Date: </b><?php echo $curr; ?></td>
                         <td style="height: 40px;">&nbsp;<b>Section: </b><?php echo $row->Section; ?></td>
                      </tr>
                    </tbody>
                </table>
                <table border="1px solid black" align="center" width="60%">
                  <tr>
                  <td class="table-body" colspan="3">Please be informed that you have incurred <b style="font-size: 20px; color: red;"><u><?php echo $_GET['num']; ?></u></b> No. Of Absences in this Subject. May this serve as a <b style="color : red;">WARNING</b> that should you accumulate more than the allowable absences, you will receive a NOTICE OF FAILURE DUE TO ABSENCES (AF). <br /><br />

                  You are advised to consult/verify this matter iwth your subject teacher. <br /><br />

                  Thank you very much! <br />
                  </td>
                  </tr> 
                </table>
                <table border="1px solid black" align="center" width="60%">
                <tr>
                <?php
                $idtk = $this->session->userdata('username');
                $code = $_GET['code'];  
                $sqli = $this->db->query("SELECT * FROM teacher WHERE Faculty_ID='$idtk'");

                $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code' AND role='2'");

                $trya = $sqli->row();

                $q = $query->row();

                $idw = $q->Students_FK;

                $result = $this->db->query("SELECT * from students WHERE Student_ID='$idw'");

                $wa = $result->row();

                $final = $wa->First_Name . ' ' . $wa->Last_Name;

                $nametk = $trya->First_Name . ' ' . $trya->Last_Name;
                ?>
                <th colspan="2" class="table-body" style="text-align: center;"><b style="float: left; padding-left: 20px;">Recorded by: <br/><u><?php echo $final; ?></u></b><b style="font-size: 15px; float: right; padding-right: 30px;">Certified Correct by:</b></b><br /><b style="font-size: 15px; float: right; padding-right: 40px;"><u><?php echo $nametk; ?></u></b><br/><input style="float: right; margin-right: 40px;" type ="submit" name="update" value="Approved" class="btn btn-success"/><br /><b style="float: left;">OSA-SA-01</b></th>
                </tr>
                </table>
              </form>
              </div>
            </div>
          </div>

        </div>
 <div id="content-wrapper" style="background-color: #dfe3ee;">
        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb" >
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes</a>
            </li>

          </ol>

          <button class="btn btn-primary" data-toggle = "modal" data-target = "#myModal">
					 Add a class
		  </button>
			
		<!-- Modal -->
<div class = "modal fade" id = "myModal" tabindex = "-1" role = "dialog" 
   aria-labelledby = "myModalLabel" aria-hidden = "true">
   
   <div class = "modal-dialog">
      <div class = "modal-content">
         
         <div class = "modal-header">
         	<h3 class = "modal-title" id = "myModalLabel" style="font-family: 'Poppins', sans-serif;">
               Create a class
            </h3>
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            
            
         </div>
         
         <div class = "modal-body">
		   <?php echo form_open('teachers/add_subjects'); ?>
		   <label style="font-family: 'Poppins', sans-serif;"><b>Semester</b></label>
		   <label style="font-family: 'Poppins', sans-serif; padding-left: 170px;"><b>School Year</b></label><br />&nbsp;&nbsp;&nbsp;&nbsp;
		   <!-- Semester -->
		   <input type="checkbox" name="sem" class="limited" onclick="return limitchex(this,1)" value="1st Semester" />1st &nbsp;&nbsp;&nbsp;
		   <input type="checkbox" name="sem" class="limited" onclick="return limitchex(this,1)" value="2nd Semester" />2nd &nbsp;&nbsp;&nbsp;
		   <input type="checkbox" name="sem" class="limited" onclick="return limitchex(this,1)" value="Summer Semester" />Summer &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		   <!-- School Year -->
		   <input type="text" name="sy1" id="sy1" size="8" required style="border-radius: 5px;"> to 
       <input type="text" name="sy2" id="sy2" size="8" required style="border-radius: 5px;"><br /><br />

       <label style="font-family: 'Poppins', sans-serif;"><b>Subject code</b></label>
       <label style="font-family: 'Poppins', sans-serif; padding-left: 45px;"><b>Section</b></label>
       <label style="font-family: 'Poppins', sans-serif; padding-left: 85px;"><b>Room</b></label>
       <br />
        <!-- Subject Code & Section, room-->
		   <input type="text" name="Subject_Code" id="Subject_Code" size="15" required style="border-radius: 5px;"> &nbsp;&nbsp;&nbsp;
		   <input type="text" name="Section" id="Section" size="15" required style="border-radius: 5px;">  &nbsp;&nbsp;
       <input type="text" name="room" id="room" size="15 " required style="border-radius: 5px;"><br />
		   <label><b>Subject name</b></label><br />
		   <!-- Subject Name -->
		   <input type="text" name="Subject_Name" id="Subject_Name" size="57" required style="border-radius: 5px;"><br />
		  	
		   <label><b>Allowable Abscences</b></label><br />
		   <input type="text" name="abs" id="abs" size="15" style="border-radius: 5px;"> &nbsp;&nbsp;&nbsp;&nbsp;
		   <?php if($this->session->flashdata('error_update') == TRUE) { ?> <!-- Automatically Open when error found-->
		   	<script type="text/javascript">
    		$(window).on('load',function(){
        	$('#myModal').modal('show');});
			</script>
			<?php } ?>
			<?php echo $this->session->flashdata('error_update'); ?> <!-- Display Error Message -->
         <div class = "modal-footer">
         	 <button type= "submit" id="submit" name="submit" class = "btn btn-primary">
               Submit changes
            </button>
            <button type="button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
           
		<?php echo form_close(); ?>
         </div>
         </div>
		
         
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
  
</div><!-- /.modal -->

      </br>
      </br> 

<div class="row">
	<?php
          
    $user = $this->session->userdata('username');

    $query = $this->db->query("SELECT * from subjects WHERE Teacher_FK='$user'");

    $record = array();

    foreach($query->result_array() as $row)
    {
      $record[] = $row;
    }

    
    ?>

    <?php foreach($record as $rec){?>
      <!-- Icon Cards-->
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-book-open"></i>
                  </div>
                  <div class="mr-5"><a href="<?php echo base_url('teachers/teachers_attendance'); ?>?code=<?php echo $rec['ClassCode'];?>" class="text-white"><b style="font-size: 20px;"><?php echo $rec['Subject_Code'] . ' - ' . $rec['Section']; ?></b></a></br><?php echo $rec['Subject_Name']; ?><br /><?php echo $rec['room'];?></div>
                </div>
                <div class="card-footer text-white clearfix small z-1">
                  <span class="float-left" style="font-size: 15px;"><b>Subject Key: </b><?php echo $rec['ClassCode']; ?></span>
                </div>
              </div>
            </div>
            
        
        <?php } ?>
        </div>
      
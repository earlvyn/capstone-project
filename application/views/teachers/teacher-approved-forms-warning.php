 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

           <?php
        
          $code = $_GET['code'];

          

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
             </li>
            <li class="breadcrumb-item active">Approved Forms</li>
          </ol>


          <ul class="navbar-nav ml-auto ml-md-0">
           <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <button class="btn btn-primary">Form Type</button>
          </a>
          <div class="dropdown-menu dropdown-menu-left" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?php echo base_url('teachers/teachers_approved_forms_daily?code=' . $code); ?>">Daily Attendance Monitoring Form</a>
            <a class="dropdown-item" href="<?php echo base_url('teachers/teachers_approved_forms_warning?code=' . $code); ?>">Notice of Warning on Attendance</a>
            <a class="dropdown-item" href="#">Notice of AF on Attendance</a>
        </li>
      </ul>

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
             Notice of Warning on Attendance</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" align="center">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>ID Number</th>
                      <th>Name</th>
                      <th>No. of Absent</th>
                      <th>View Record</th>       
                    </tr>
                  </thead>
                 <tbody class="table-body" align="center">
                    <?php 

                    $code1 = $_GET['code'];

                    $id = $this->session->userdata("username");

                    $query = $this->db->query("SELECT `Date`, osa_status, Students_fk, Teacher_fk from forms where Teacher_fk='$id' AND ClassCode='$code1' AND Form_Type='warning' ORDER BY `Date`");

                    $i = 1;
                    foreach($query->result_array() as $try ) {

                    $date = $try['Date'];

                    $idn = $try['Students_fk'];

                    $tk = $try['Teacher_fk'];

                    $year = substr($id,1,4); //To get the year
                    $mid = substr($id,-5,-4); //To get the 5th character
                    $last = substr($id,6,4); //To get the last 4 character

                    $final = $year . '-' . $mid . '-' . $last; // e.g 20131-1-0183 

                    $sql = $this->db->query("SELECT * FROM students WHERE Student_ID='$idn'");

                    $rec = $sql->row_array();

                    $fname = $rec['First_Name'];
                    $mname = substr($rec['Middle_Name'],0,1);
                    $lname = $rec['Last_Name'];

                    $name = $fname . ' ' . $mname . '. ' . $lname;

                    $curr = date("F j, Y (l)", strtotime($date));

                    $sql1 = $this->db->query("SELECT * from forms WHERE Form_Type='daily' AND osa_status='1' AND ClassCode='$code' AND Teacher_fk='$tk'");

                      $w = 0;

                      foreach($sql1->result_array() as $row)
                      {
                      if($row['Students_fk'] == $idn)
                      {
                        if($row['Remark'] == 'Absent')
                        {
                          $w++;
                        }
                      }
                      }


                    if($try['osa_status'] == '3')
                    {
                    ?>
                    <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $final; ?></td>
                    <td><?php echo $name; ?></td>
                    <td><?php echo $w; ?></td>
                    <td align="center"><a href="<?php echo base_url('teachers/teachers_approved_forms_warning_view?code=' . $code . '&date=' . $date . '&id=' . $idn . '&num=' . $w) ?>""><i class="fas fa-eye"> View</i></a></td>
                  </tr>
                  <?php $i++; }} ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
          

        
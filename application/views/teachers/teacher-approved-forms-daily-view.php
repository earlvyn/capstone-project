 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

           <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_approved_forms_daily?code=' . $code); ?>">Approved Forms</a>
            </li>
             </li>
            <li class="breadcrumb-item active">Approved Form View</li>
          </ol>

        
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Daily Attendance Monitoring Form</div>
            <div class="card-body">
              <div class="table-responsive">
                <table border="1px solid black" align="center" width="60%">
                    <thead class="table-heading">
                    <tr>
                    <th colspan="2" style="text-align: center; height: 70px;">Daily Attendance Monitoring Form</th>
                    </tr>
                    </thead>
                    <tbody class="table-body">
                    <?php

                      $code = $_GET['code'];
                      $date = $_GET['date'];
                      
                      $result = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

                      $curr = date("F j, Y (l)", strtotime($date));

                      $row = $result->row();
                      ?>
                      <tr>
                        <td style="height: 40px;">&nbsp;<b>Subject Code: </b><?php echo $row->Subject_Code;?></td>
                        <td style="height: 40px;">&nbsp;<b>Section: </b><?php echo $row->Section; ?></td>
                      </tr>
                       <tr>
                         <td colspan="0" style="height: 50px;">&nbsp;<b>Date: </b><?php echo $curr; ?></td>
                         <td colspan="0" style="height: 50px;">&nbsp;<b>ClassCode: </b><?php echo $code; ?></td>
                      </tr>
                    </tbody>
                </table>
                <table border="1px solid black" align="center" width="60%">
                  <tr>
                  <th class="table-body" style="text-align: left; height: 40px;">&nbsp;Name of Student(Last Name, First Name, M.I.)</th>
                  <th class="table-body" style="text-align: center; height: 40px;">Remark</th>
                  </tr>
                  <?php 
                        $i = 1;
                        $code2 = $_GET['code'];
                        $date2 = $_GET['date'];
                        $ida = $this->session->userdata('username');

                        $query1 = $this->db->query("SELECT * FROM attendance_record WHERE ClassCode_fk='$code2' AND Teacher_fk='$ida' AND `Date`='$date2'");

                        foreach($query1->result_array() as $rows)
                        {
                          $stat = $rows['Status'];
                
                            if($stat== 'Absent' || $stat == 'Late')
                            {
                              $id2 = $rows['Students_fk'];

                              $result1 = $this->db->query("SELECT * FROM students WHERE Student_ID='$id2'");

                              $q = $result1->row();

                              $fname = $q->First_Name;
                              $mname = substr($q->Middle_Name,0,1);
                              $lname = $q->Last_Name;

                              $name = '<b>' . $lname . '</b>, ' . $fname . ' ' . $mname . '.';  

                   ?>
                        <tr>
                        <td class="table-body" style="text-align: left; height: 40px;">&nbsp;<?php echo $i . '. ' . $name; ?></td>
                        <td class="table-body" style="text-align: center; height: 40px;"><?php echo $stat; ?></td>
                        </tr>
                        <?php $i++; }}?>
                </table>
                <table border="1px solid black" align="center" width="60%">
                <tr>
                <?php
                $idtk = $this->session->userdata('username');  
                $sqli = $this->db->query("SELECT * FROM teacher WHERE Faculty_ID='$idtk'");

                $trya = $sqli->row();

                $nametk = $trya->First_Name . ' ' . $trya->Last_Name;
                ?>
                <th class="table-body" style="text-align: center;">Recorded/Certified by: <br/><u><?php echo $nametk; ?></u><br /><b style="font-size: 15px">Teacher</b><br /><b style="float: left;">OSA-SA-01</b></th>
                </tr>
                </table>
            
              </div>
            </div>
          </div>

        </div>
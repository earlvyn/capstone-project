 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

           <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
            $max = $row['max_absent'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
            <li class="breadcrumb-item active">View Attendance</li>
          </ol>

          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              View Attendance</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>ID#</th>
                      <th>Name</th>
                      <th>Status</th>
                      <th>Excuse Letter</th>       
                    </tr>
                  </thead>
                  <tbody class="table-body" align="center">
                  <?php
                  $code = $_GET['code'];
                  $date = $_GET['date'];
                  $x = 1;
                  $i = 0;
                  $query = $this->db->query("SELECT `Date`, Status, Excuse_Letter, Students_fk from attendance_record where ClassCode_fk = '$code' AND `Date`='$date'");

                  foreach($query->result_array() as $row)
                  {
                    $id = $row['Students_fk'];
                    $ex = $row['Excuse_Letter']; 
                  
                    $stat = $row['Status'];

                    $result = $this->db->query("SELECT CONCAT(First_Name,' ', Last_Name) AS name, Course, Student_ID FROM students WHERE Student_ID='$id' ORDER BY Last_Name ASC");

                    foreach($result->result_array() as $rows)
                    {
                       $idn = $rows['Student_ID'];
                  

                        $year = substr($idn,1,4); //To get the year
                        $mid = substr($idn,-5,-4); //To get the 5th character
                        $last = substr($idn,6,4); //To get the last 4 character

                        $final = $year . '-' . $mid . '-' . $last; // e.g 20131-1-0183


                        echo '<tr>';
                        echo '<td>' . $x . '</td>';
                        echo '<td>' . $final . '</td>';
                        echo '<td>' . $rows['name'] . '</td>';
                    

                    $query1 = $this->db->query("SELECT * from attendance_record WHERE ClassCode_fk='$code'");
                    $w = 0;
                    foreach($query1->result_array() as $try)
                    {
                        if($try['Students_fk'] == $idn)
                        {
                            if($try['Status'] == 'Absent' )
                            {
                              $w++;
                            }
                        }
                    }
                    }


                    
                  if($w == $max+1) {
                  
                  ?>
                  <td style="background-color: red;">
                  <input type="hidden" name="Status<?php echo $i; ?>" value="AF"/> AF
                  </td>
                  
                  <?php } else { ?>
                  <td>
                  <input type="radio" name="Status<?php echo $i; ?>" value="Present" <?php echo ($stat=='Present')?'checked':'' ?> disabled> Present
                  <input type="radio" name="Status<?php echo $i; ?>" value="Absent" <?php echo ($stat=='Absent')?'checked':'' ?> disabled> Absent
                  <input type="radio" name="Status<?php echo $i; ?>" value="Late"<?php echo ($stat=='Late')?'checked':'' ?> disabled> Late
                  <input type="radio" name="Status<?php echo $i; ?>" value="Excused"<?php echo ($stat=='Excused')?'checked':'' ?> disabled> Excused
                  </td>

                  <?php
                  } 
                  if(empty($ex))
                  {
                    echo '<td> None </td>';
                  }
                  else
                  {
                    echo '<td>';
                  ?>
                   <a href="<?php echo base_url('images\\' . $ex); ?>" target="_blank" onclick="window.open('images\\' . '$ex', 'popup', 'height=800, width=800'); return false;"><?php echo $ex; ?></a>
                  <?php 
                    echo '</td>';
                  }

                 ?>

                <?php $x++; $i++; } ?>
                <?php echo '</tr>'; ?>
                </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
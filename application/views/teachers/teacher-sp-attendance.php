 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

           <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance_option?code=' . $code); ?>">Select View</a>
            </li>
             </li>
            <li class="breadcrumb-item active">Seat Plan View</li>
          </ol>

          <?php 
            date_default_timezone_set('Asia/Manila');
            $date = date("F j, Y (l)");
            
            echo "<a class='subjectn' style='float: left; margin-right: 80px; font-size: 20px;'>$date</a>";

            $code = $_GET['code'];

            $sql = $this->db->query("SELECT * from subjects WHERE ClassCode='$code'");

            $result = $sql->row();

            $count = $result->max_absent;

            ?>

            <br /><br />
          <div class="card mb-3" >
            <div class="card-header" >
              <i class="fas fa-table" ></i>
             Add Record(Seat Plan View) <?php echo '<b style="float: right; font-size: 20px;">Allowable Abscences: ' . $count . '</b>'; ?></div>
            <div class="card-body" >
<form action="<?php echo base_url('teachers/record_attendance_sp?code=' . $code); ?>" method="POST">
  <div align="left">
                  <input type ="submit" name="insert" value="Add Record" class="btn btn-success"/>
                </div>
<div class="chairs_right">
        <?php 
        echo "<table border='1' width='100%' height='100%'><br />";
        $arow = '';
        $acol = '';
        $code = $_GET['code'];
        $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

        foreach($query->result_array() as $rows)
        {
          $acol = $rows['acol'];
          $arow = $rows['arow'];
        }
        $i = $acol * $arow + 1;
        $x = $i;
        for ($row=0; $row < $arow; $row++) 
          { 
          echo "<tr>";

            for ($col = 0; $col < $acol; $col ++) 
            { 
              $x--;

              $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code' AND seat_no='$x'");

              $rowz = $query->unbuffered_row('array');

              if($x == $rowz['seat_no'])
              {
                $idn = $rowz['Students_FK'];

                $result = $this->db->query("SELECT * FROM students WHERE Student_ID='$idn'");

                $name = $result->unbuffered_row('object');

                $query1 = $this->db->query("SELECT * from attendance_record WHERE ClassCode_fk='$code'");

                $w = 0;
                $p = 0;
                foreach($query1->result_array() as $try)
                {
                    if($try['Students_fk'] == $idn)
                    {
                        if($try['Status'] == 'Absent' )
                        {
                              $w++;
                        }
                        if($try['Status'] == 'Late' )
                        {
                              $p++;
                        }
                    }
                }

              if($w == $count+1) {
              ?>
              
              <td style="background-color: red;">
              <div>
              <a href="#myModal<?php echo $x;?>" data-toggle="modal" style="color: black"><?php echo $name->Last_Name; ?></a><br />
                  <input type="hidden" name="Status<?php echo $x; ?>" value="AF"/>        
              </div>
              </td>
              <?php } elseif($w == $count) {?>
              <td style="background-color: yellow;">
              <div>
              <a href="#myModal<?php echo $x;?>" data-toggle="modal" style="color: black"><?php echo $name->Last_Name; ?></a><br />
                  <input type="radio" name="Status<?php echo $x; ?>" value="Present" checked>P
                  <input type="radio" name="Status<?php echo $x; ?>" value="Absent">A
                  <input type="radio" name="Status<?php echo $x; ?>" value="Late">L   
             
              </div>
              </td>
              <?php } else { ?>
              <td class="color_td">
              <div>
              <a href="#myModal<?php echo $x;?>" data-toggle="modal" style="color: black"><?php echo $name->Last_Name; ?></a><br />
                  <input type="radio" name="Status<?php echo $x; ?>" value="Present" checked>P
                  <input type="radio" name="Status<?php echo $x; ?>" value="Absent">A
                  <input type="radio" name="Status<?php echo $x; ?>" value="Late">L   
              </div>
              </td>
              <?php } ?>
              <!-- Modal for student information -->
              <div class="modal fade" id="myModal<?php echo $x; ?>" role="dialog">
                  <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                      <div class="modal-header">
                          <h4 class="modal-title" align="center">STUDENT INFORMATION</h4>
                      </div>
                    <div class="modal-body" align="left">
                        <?php 
                        
                        echo "<p><b>Name: </b><br />" . $name->Last_Name . ", " . $name->First_Name . "</p>";
                        echo "<p><b>Year & Course: </b><br />" . $name->Year . " - " . $name->Course . "</p>";
                        echo "<p><b>Absences: " . $w . "</b><br /></p>";
                        echo "<p><b>Late: " . $p . "</b><br /></p>";
                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                    </div>
                    </div>
                </div>
              </div>       
              
              <?php
              } 
              else
              {
                
                echo "<td class='color_td2'>Seat No.<br />" . $x . "</td>";
              }
              
            }
          echo "</tr>";

          }
        echo "</table>";
        ?>
        <b>RIGHT</b>
        </div>

<div class="chairs_left">
  <?php 
        echo "<table border='1'  width='100%' height='100%'><br />";

        $brow = '';
        $bcol = '';
        
        $code = $_GET['code'];
        $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

        foreach($query->result_array() as $rows)
        {
          $arow = $rows['arow'];
          $acol = $rows['acol'];

          $bcol = $rows['bcol'];
          $brow = $rows['brow'];
        }
        $w = $acol*$arow+1;
        $i = $brow * $bcol + $w;
        $x = $i;
        for ($row=0; $row < $brow;$row++) 
          { 
          echo "<tr>";

            for ($col = 0; $col < $bcol; $col ++) 
            { 
              $x--;
              
              $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code' AND seat_no='$x'");

              $rowz = $query->unbuffered_row('array');

              if($x == $rowz['seat_no'])
              {
                $idn = $rowz['Students_FK'];

                $result = $this->db->query("SELECT * FROM students WHERE Student_ID='$idn'");

                $name = $result->unbuffered_row('object');

                $query1 = $this->db->query("SELECT * from attendance_record WHERE ClassCode_fk='$code'");
                $w = 0;
                $p = 0;
                foreach($query1->result_array() as $try)
                {
                    if($try['Students_fk'] == $idn)
                    {
                        if($try['Status'] == 'Absent' )
                        {
                              $w++;
                        }
                        if($try['Status'] == 'Late' )
                        {
                              $p++;
                        }
                    }
                }

              if($w == $count+1) {
              ?>
              
              <td style="background-color: red;">
              <div>
              <a href="#myModal<?php echo $x;?>" data-toggle="modal" style="color: black "><?php echo $name->Last_Name; ?></a><br />
                <input type="hidden" name="Status<?php echo $x; ?>" value="AF"/>   
              </div>
              </td>
              <?php } elseif($w == $count) {?>
              <td style="background-color: yellow;">
              <div>
              <a href="#myModal<?php echo $x;?>" data-toggle="modal" style="color: black"><?php echo $name->Last_Name; ?></a><br />
                  <input type="radio" name="Status<?php echo $x; ?>" value="Present" checked>P
                  <input type="radio" name="Status<?php echo $x; ?>" value="Absent">A
                  <input type="radio" name="Status<?php echo $x; ?>" value="Late">L   
             
              </div>
              </td>
              <?php } else { ?>
              <td class="color_td">
              <div>
              <a href="#myModal<?php echo $x;?>" data-toggle="modal" style="color: black"><?php echo $name->Last_Name; ?></a><br />
                  <input type="radio" name="Status<?php echo $x; ?>" value="Present" checked>P
                  <input type="radio" name="Status<?php echo $x; ?>" value="Absent">A
                  <input type="radio" name="Status<?php echo $x; ?>" value="Late">L   
              
              </div>
              </td>
              <?php } ?>
              <!-- Modal for student information -->
              <div class="modal fade" id="myModal<?php echo $x; ?>" role="dialog">
                  <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                      <div class="modal-header">
                          <h4 class="modal-title" align="center">STUDENT INFORMATION</h4>
                      </div>
                    <div class="modal-body" align="left">
                        <?php 
                        
                        echo "<p><b>Name: </b><br />" . $name->Last_Name . ", " . $name->First_Name . "</p>";
                        echo "<p><b>Year & Course: </b><br />" . $name->Year . " - " . $name->Course . "</p>";
                        echo "<p><b>Absences: " . $w . "</b><br /></p>";
                        echo "<p><b>Late: " . $p . "</b><br /></p>";
                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                    </div>
                    </div>
                </div>
              </div>

              <?php
              } 
              else
              {
                
                echo "<td class='color_td2'>Seat No.<br />" . $x . "</td>";
              }
              
            }
          echo "</tr>";

          }
        echo "</table>";
        ?>
        <b>LEFT</b>
        </div>


      </div>
        <br />
        <div class = "teachers_table_t">TEACHER'S TABLE</div>
        <br />
        <br />
        <br />


    </div>

  </form>


</div>
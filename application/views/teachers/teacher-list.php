 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

          <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
             </li>
            <li class="breadcrumb-item active">Class List</li>
          </ol>
          <?php 

          $code = $_GET['code'];

          $sql = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");
          $i = 0;
          foreach($sql->result_array() as $row)
          {
            $i++;
          }

          ?>
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Class List View <b style="float: right; font-size: 18px;">Enrolled Students: <?php echo $i;?></b>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>ID #</th>
                      <th>Name</th>
                      <th>Course</th>     
                    </tr>
                  </thead>
                      <?php
          
                    $code = $_GET['code'];
                
                    $result = $this->db->query( "SELECT * FROM students, classes WHERE Students_FK=Student_ID AND ClassCode_FK='$code' ORDER BY Last_Name ASC");
                    
                    $record = array();
                    $i=1;
                    foreach($result->result_array() as $row)
                    {
                      $record[] = $row;
                    }
                    
                    ?>
                    
                    <?php foreach($record as $rec) 
                    {

                      $id = $rec['Student_ID'];

                      $year = substr($id,1,4); //To get the year
                      $mid = substr($id,-5,-4); //To get the 5th character
                      $last = substr($id,6,4); //To get the last 4 character

                      $final = $year . '-' . $mid . '-' . $last; // e.g 20131-1-0183 

                      $fname = $rec['First_Name'];
                      $mname = substr($rec['Middle_Name'],0,1);
                      $lname = $rec['Last_Name'];

                      $name = '<b>' . $lname . '</b>, ' . $fname . ' ' . $mname . '.';

                     ?>
                    <tbody class="table-body">
                    <tr>
                    <?php echo '<td align="center"><i class="fas fa-user-circle" style="font-size: 24px;"></i></td>';?>
                    <?php echo '<td>' . $final . '</td>';?>
                    <?php echo '<td align="left">' . $name . '</td>';?>
                    <?php echo '<td align="left">' . $rec['Course'] . '</td>'; ?>
                    </tr>
                    </tbody>
                    <?php $i++; } ?>
                </table>
              </div>
            </div>
          </div>

        </div>
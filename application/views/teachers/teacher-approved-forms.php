<div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

          <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];

          }

          echo "<h2 class='subjectn' style='text-align: center'>$name</h2>";
          echo "<p style='text-align: center;'>$s_code - $section</p>";
          echo "<p style='text-align: center;'>S/Y $syear $semester</p>";
         
          
          ?>


          <ul class="navbar-nav ml-auto ml-md-0">
           <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <button class="btn btn-primary">Form Type</button>
          </a>
          <div class="dropdown-menu dropdown-menu-left" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?php echo base_url('teachers/teachers_approved_forms_daily?code=' . $code); ?>">Daily Attendance Monitoring Form</a>
            <a class="dropdown-item" href="#">Notice of Warning on Attendance</a>
            <a class="dropdown-item" href="#">Notice of AF on Attendance</a>
        </li>
      </ul>

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">MyClasses</a>
            </li>
             <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance</a>
            </li>
            <li class="breadcrumb-item active">Approved Forms
            </li>
          </ol>

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Approved Forms</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>Subject</th>
                      <th>Subject Name</th>
                      <th>Section</th>
                      <th>Instructor</th>
                      <th>View Record</th>       
                    </tr>
                  </thead>
                  <tbody class="table-body">
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
          

        
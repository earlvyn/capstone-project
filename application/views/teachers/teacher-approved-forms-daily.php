 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

           <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
             </li>
            <li class="breadcrumb-item active">Approved Forms</li>
          </ol>


          <ul class="navbar-nav ml-auto ml-md-0">
           <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <button class="btn btn-primary">Form Type</button>
          </a>
          <div class="dropdown-menu dropdown-menu-left" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?php echo base_url('teachers/teachers_approved_forms_daily?code=' . $code); ?>">Daily Attendance Monitoring Form</a>
            <a class="dropdown-item" href="<?php echo base_url('teachers/teachers_approved_forms_warning?code=' . $code); ?>">Notice of Warning on Attendance</a>
            <a class="dropdown-item" href="#">Notice of AF on Attendance</a>
        </li>
      </ul>

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
             Daily Attendance Monitoring Form</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" align="center">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>Date</th>
                      <th>View Record</th>       
                    </tr>
                  </thead>
                 <tbody class="table-body" align="center">
                    <?php 

                    $code1 = $_GET['code'];

                    $id = $this->session->userdata("username");

                    $query = $this->db->query("SELECT distinct `Date`, osa_status from forms where Teacher_fk='$id' AND ClassCode='$code1' ORDER BY `Date`");

                    $i = 1;
                    foreach($query->result_array() as $try ) {

                    $date = $try['Date'];

                    $curr = date("F j, Y (l)", strtotime($date));

                    if($try['osa_status'] == '1')
                    {
                    ?>
                    <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $curr; ?></td>
                    <td align="center"><a href="<?php echo base_url('teachers/teachers_approved_forms_daily_view?code=' . $code . '&date=' . $date) ?>""><i class="fas fa-eye"> View</i></a></td>
                  </tr>
                  <?php $i++; }} ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
          

        
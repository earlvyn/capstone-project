<?php

$code = $_GET['code'];

$sql = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

foreach($sql->result_array() as $row)
{
	$subcode = $row['Subject_Code'];
	$subname = $row['Subject_Name'];
	$section = $row['Section'];
	$tk = $row['Teacher_FK'];
  $room = $row['room'];
  $syear = $row['syear'];
  $semester = $row['semester'];

	$query = $this->db->query("SELECT * FROM teacher WHERE Faculty_ID='$tk'");

	$rows = $query->row();

	$classname = $subcode . ' - ' . $section . ' ' . $subname;
	$teachername = $rows->Last_Name . ', ' . $rows->First_Name;

	
}

 $sql1 = $this->db->query("SELECT distinct `Date` FROM attendance_record WHERE ClassCode_fk='$code' AND Teacher_fk='$tk'"); 
        

if(isset($_POST["create_pdf"])) {
      $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
      $obj_pdf->SetCreator(PDF_CREATOR);  
      $obj_pdf->SetTitle("Attendance Summary for " . $classname);  
      $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
      $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
      $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
      $obj_pdf->SetDefaultMonospacedFont('helvetica');  
      $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
      $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
      $obj_pdf->setPrintHeader(false);  
      $obj_pdf->setPrintFooter(false);  
      $obj_pdf->SetAutoPageBreak(TRUE, 10);  
      $obj_pdf->SetFont('helvetica', '', 12);  
      $obj_pdf->AddPage();  
      $content = '';  
      $content .= '  
      <h2 align="center">Attendance Summary for ' . $semester . ' SY ' . $syear . '</h2>
      <table border="0">
      <tr>
      		<th align="center"><b>Subject Code</b></th>
          <th align="center"><b>Section</b></th>
          <th align="center"><b>Subject Name</b></th>
          <th align="center"><b>Room</b></th>
          <th align="center"><b>Instructor</b></th>
      </tr>
      <br />
      <tr>
      		<td align="center">' . $subcode . '</td>
          <td align="center">' . $section . '</td>
          <td align="center">' . $subname . '</td>
          <td align="center">' . $room . '</td>
          <td align="center">' . $teachername . '</td>


      </tr>
      </table>
      <br /> <br />';
      foreach($sql1->result_array() as $qwe) {
      		
      		$date = $qwe['Date'];

  			$curr = date("F j, Y (l)", strtotime($date));
      		$content .= '
      		<table border="1" cellspacing="0" cellpadding="3">  
           <tr>  
                <th width="25%" align="center">Date</th>  
                <th width="50%" align="left">Name of Student</th>  
                <th width="25%" align="center">Remarks</th>    
           </tr>
           <tr>
           		<td width="25%" align="center">' . $curr .'</td>';

           $sql2 = $this->db->query("SELECT * FROM attendance_record WHERE ClassCode_fk='$code' AND Teacher_fk='$tk' AND `Date`='$date'");

  		  $content .= '<td width="50%" align="left">';
  		  $i = 0;
          while($tra = $sql2->unbuffered_row('array'))
          {
          		
          		if($tra['Status'] == 'Absent' || $tra['Status'] == 'Late')
          		{
          			$i++;
          			$awe = $tra['Students_fk'];
          			$n = $this->db->get_where('students', array('Student_ID' => $awe))->row();

          			$namp = $n->Last_Name  . ', ' . $n->First_Name;
          			$content .=  '<table style="border-bottom:1pt solid black"><tr><td>' . $i . '. ' . $namp . '</td></tr></table><br />';
          		}
          		

          }
          $content .= '</td> <td width="25%" align="center">';

          $sql3 = $this->db->query("SELECT * FROM attendance_record WHERE ClassCode_fk='$code' AND Teacher_fk='$tk' AND `Date`='$date'");

          while($trya = $sql3->unbuffered_row('array'))
          {
          		if($trya['Status'] == 'Absent' || $trya['Status'] == 'Late')
          		{
          			$content .= '<table style="border-bottom:1pt solid black"><tr><td>' . $trya['Status'] . '</td></tr></table><br />';
          		}
           		
          }
          $content .= '</td>';		
           
          $content .= '</tr>
          		 </table><br /><br />';
          
           
       }
   
   	 
      $obj_pdf->writeHTML($content);  
      $obj_pdf->Output('sample.pdf', 'I');  
      
  }
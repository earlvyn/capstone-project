<div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

          <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
            <li class="breadcrumb-item active">Configure Seat plan</li>
          </ol>

         

        <button class="btn btn-primary" data-toggle = "modal" data-target = "#myModal_configure">
        Set seat
        </button>


        <button class="btn btn-primary" data-toggle = "modal" data-target = "#assignModal">
          Assign Seat
          </button>

          

          <br /><br />


          <?php 

          $code = $_GET['code'];

          $sql = $this->db->get_where('classes', array('ClassCode_FK' => $code))->row();;

          $lrow = $sql->arow;
          $lcol = $sql->acol;
          $rrow = $sql->brow;
          $rcol = $sql->bcol;

          $l = $lrow * $lcol;
          $r = $rrow * $rcol;

          $sum = $l + $r;

          ?>

          <!-- Modal -->
        <div class="modal fade" id="myModal_configure" role="dialog">
            <div class="modal-dialog modal-sm">
          
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                      
                      <h4 class="modal-title">Configure Seatplan</h4>
                  </div>
                <form action="<?php echo base_url('teachers/configure_sp?code=' . $_GET['code']) ?>" method="POST">
                <div class="modal-body">
                  
                    <p><b>Left Side</b></p>
                    Row: </br>
                    <input type="number" name="lrow" min="0"></br>
                    Col: </br>
                    <input type="number" name="lcol" min="0"></br>
                    <p><b>Right Side</b></p>
                    Row: </br>
                    <input type="number" name="rrow" min="0"></br>
                    Col: </br>
                    <input type="number" name="rcol" min="0" ><br />
                  
                </div>
                <div class="modal-footer">
                    <button type= "submit" id="save" name="save" class = "btn btn-primary">Save</button>
                </div>
                </div>
                </form>
            </div>

      </div>





          

          <!-- Assign Seat Modal -->
          <div class="modal fade" id="assignModal" role="dialog">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" align="center">Assign Seat</h4>
              </div>
            <div class="modal-body">
              <div class="container">
                
                  <div class="form-group">
                    <form action="<?php echo base_url('teachers/assign_seat'); ?>?code=<?php echo $_GET['code']; ?>" method="POST">
                    <label for="sel1"><b>Class List: </b></label>
                    
                      <?php
          
                      $code = $_GET['code']; 
                  
              
                      $result = $this->db->query("SELECT Last_Name, Course, Student_ID FROM students, classes WHERE Students_FK=Student_ID AND ClassCode_FK='$code' ORDER BY Last_Name ASC");
                  
                  $record = array();
                  
                  foreach($result->result_array() as $row)
                  {
                    $record[] = $row;
                  }
                  
                  ?>
                  <select name="name">
                  <?php foreach($record as $rec) { ?>
                      <option value="<?php echo $rec['Student_ID']; ?>"><?php echo $rec['Last_Name']; ?></option>
                      <?php } ?>
                      </select></br></br>
                      <label for="id2"><b>Seat Number: </b></label>
                      <br />
                      <select name="id2" style="width: 125px;" >
                  <?php 
                  $i = 0;
                  for ($row=0; $row < $sum; $row++) { 
                    $i++ ?>
                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>    
                  <?php } ?>
                
                      </select></br>
                    
                  </div>
                
              </div>
            
            </div>

            <div class="modal-footer">
              <button type= "submit" id="save" name="save" class = "btn btn-primary">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </form>
            </div>
            
            </div>
          </div>
          </div>
<!--Chairs-->

 <div class="card mb-3" >
            <div class="card-header" >
              <i class="fas fa-table" ></i>
              Assign Seating Arrangement</div>
            <div class="card-body" >

<div class="chairs_right">
        <?php 
        echo "<table border='1' width='100%' height='100%'><br />";
        $arow = '';
        $acol = '';
        $code = $_GET['code'];
        $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

        foreach($query->result_array() as $rows)
        {
          $acol = $rows['acol'];
          $arow = $rows['arow'];
        }
        $i = $acol * $arow + 1;
        $x = $i;
        for ($row=0; $row < $arow; $row++) 
          { 
          echo "<tr>";

            for ($col = 0; $col < $acol; $col ++) 
            { 
              $x--;

              $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code' AND seat_no='$x'");

              $rowz = $query->unbuffered_row('array');

              if($x == $rowz['seat_no'])
              {
                $idn = $rowz['Students_FK'];

                $result = $this->db->query("SELECT * FROM students WHERE Student_ID='$idn'");

                $name = $result->unbuffered_row('object');

                
              
              ?>
              
              <td class="color_td">
              <div>
              <a href="#myModal<?php echo $x;?>" data-toggle="modal" style="color: black"><i class="fas fa-user-circle" style="font-size: 25px;"></i><br/><?php echo $name->Last_Name; ?></b></a><br />

              </div>
              </td>
              <!-- Modal for student information -->
              <div class="modal fade" id="myModal<?php echo $x; ?>" role="dialog">
                  <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                      <div class="modal-header">
                          <h4 class="modal-title" align="center">STUDENT INFORMATION</h4>
                      </div>
                    <div class="modal-body" align="left">
                        <?php 
                        
                        echo "<p><b>Name: </b><br />" . $name->Last_Name . ", " . $name->First_Name . "</p>";
                        echo "<p><b>Year & Course: </b><br />" . $name->Year . " - " . $name->Course . "</p>";
                      
                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                    </div>
                    </div>
                </div>
              </div>
              

              
              <?php
              } 
              else
              {
                
                echo "<td class='color_td2'>Seat No.<br />" . $x . "</td>";
              }
              
            }
          echo "</tr>";

          }
        echo "</table>";
        ?>
        <b>RIGHT</b>
        </div>

<div class="chairs_left">
  <?php 
        echo "<table border='1'  width='100%' height='100%'><br />";

        $brow = '';
        $bcol = '';
        
        $code = $_GET['code'];
        $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code'");

        foreach($query->result_array() as $rows)
        {
          $arow = $rows['arow'];
          $acol = $rows['acol'];

          $bcol = $rows['bcol'];
          $brow = $rows['brow'];
        }
        $w = $acol*$arow+1;
        $i = $brow * $bcol + $w;
        $x = $i;
        for ($row=0; $row < $brow;$row++) 
          { 
          echo "<tr>";

            for ($col = 0; $col < $bcol; $col ++) 
            { 
              $x--;

              $query = $this->db->query("SELECT * FROM classes WHERE ClassCode_FK='$code' AND seat_no='$x'");

              $rowz = $query->unbuffered_row('array');

              if($x == $rowz['seat_no'])
              {
                $idn = $rowz['Students_FK'];

                $result = $this->db->query("SELECT * FROM students WHERE Student_ID='$idn'");

                $name = $result->unbuffered_row('object');

                
              
              ?>
              
              <td class="color_td">
              <div>
              <a href="#myModal<?php echo $x;?>" data-toggle="modal" style="color: black"><i class="fas fa-user-circle" style="font-size: 25px;"></i><br/><?php echo $name->Last_Name; ?></b></a><br />
             
              </div>
              </td>

              <!-- Modal for student information -->
              <div class="modal fade" id="myModal<?php echo $x; ?>" role="dialog">
                  <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                      <div class="modal-header">
                          <h4 class="modal-title" align="center">STUDENT INFORMATION</h4>
                      </div>
                    <div class="modal-body" align="left">
                        <?php 
                        
                        echo "<p><b>Name: </b><br />" . $name->Last_Name . ", " . $name->First_Name . "</p>";
                        echo "<p><b>Year & Course: </b><br />" . $name->Year . " - " . $name->Course . "</p>";
                        
                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                    </div>
                    </div>
                </div>
              </div>


              <?php
              } 
              else
              {
                
                echo "<td class='color_td2'>Seat No.<br />" . $x . "</td>";
              }
              
            }
          echo "</tr>";

          }
        echo "</table>";
        ?>
        <b>LEFT</b>
        </div>


      </div>
      <br />
      <br />
        <div class = "teachers_table_t">TEACHER'S TABLE</div>
        <br />
        <br />
        <br />

       
    </div>


</div>




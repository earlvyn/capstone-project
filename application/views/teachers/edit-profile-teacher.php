<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--  This file has been downloaded from https://bootdey.com  -->
    <!--  All snippets are MIT license https://bootdey.com/license -->
    <title>AAMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editprofile.css">
</head>
<body>
<div class="container bootstrap snippets">
<div class="row">
  <div class="col-xs-12 col-sm-9">
      <?php echo form_open('teachers/update', "class='form-horizontal'"); ?>
      <div class="panel panel-default">
        <div class="panel-heading">
		<img src="<?php echo base_url('images/seal.png'); ?>" class="seal">
		<br />
		<br />
		<br />
		<br />
    <br />
        <h4 class="panel-title">Update Profile</h4>
        </div>
        <div class="panel-body">
		  <div class="form-group">
            <label class="col-sm-2 control-label">First Name</label>
            <div class="col-sm-10">
              <input id="fname" name="fname" type="text" class="form-control" placeholder="e.g Jose Bryan">
            </div>
          </div>
		   <div class="form-group">
            <label class="col-sm-2 control-label">Middle Name</label>
            <div class="col-sm-10">
              <input id="mname" name="mname" type="text" class="form-control" placeholder="e.g Monares">
            </div>
          </div>
		   <div class="form-group">
            <label class="col-sm-2 control-label">Last Name</label>
            <div class="col-sm-10">
              <input id="lname" name="lname" type="text" class="form-control" placeholder="e.g Galvo">
            </div>
          </div>
          <?php echo $this->session->flashdata('error_update'); ?>
			<div class="form-group1">
            <div class="col-sm-10 col-sm-offset-2">
              <!--<button id="update" name="submit" type="submit" class="btn btn-primary">Update</button>-->
              <?php echo form_submit('update', 'Update', "class='btn btn-primary'"); ?> 
              <a href="<?php echo base_url('login/logout'); ?>" class="btn btn-default">Cancel</a>
            </div>
          </div>	
        </div>
      </div>

          </div>
          
        </div>
      </div>
    <?php echo form_close(); ?>
  </div>
</div>



</body>
</html>


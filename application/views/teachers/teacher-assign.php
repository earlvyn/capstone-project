 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

          <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
             </li>
            <li class="breadcrumb-item active">Assign Beadle</li>
          </ol>
      

          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Assign Beadle View</div>
            <div class="card-body">
              <div class="table-responsive">
                <form action="<?php echo base_url('teachers/assign_b');?>?code=<?php echo $_GET['code']; ?>" method="POST">
                <table class="table table-bordered" width="100%" cellspacing="0">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>ID #</th>
                      <th>Name</th>
                      <th>Course</th>
                      <th>Role</th>     
                    </tr>
                  </thead>
                    <?php
      
                    $code = $_GET['code'];
                
                    $result = $this->db->query("SELECT * FROM students, classes WHERE Students_FK=Student_ID AND ClassCode_FK='$code' ORDER BY Last_Name ASC");
                    $i = 1;     
                        
                    while($row = $result->unbuffered_row('array')) // or foreach($result->result_array() as $row)
                    {
                      
                    $id = $row['Student_ID']; 
                      
                    $query = $this->db->query("SELECT role FROM classes where ClassCode_FK='$code' AND Students_FK='$id'");
                    
                    $rows = $query->row_array();
                    
                    $value = $rows['role'];
                    
                    $year = substr($id,1,4); //To get the year
                    $mid = substr($id,-5,-4); //To get the 5th character
                    $last = substr($id,6,4); //To get the last 4 character

                    $final = $year . '-' . $mid . '-' . $last; // e.g 20131-1-0183  

                    $fname = $row['First_Name'];
                    $mname = substr($row['Middle_Name'],0,1);
                    $lname = $row['Last_Name'];

                    $name = '<b>' . $lname . '</b>, ' . $fname . ' ' . $mname . '.';
                    
                    ?>
                    <tbody class="table-body">
                    <tr>
                      <?php echo '<td align="center"><i class="fas fa-user-circle" style="font-size: 24px;"></i></td>';?>
                      <?php echo '<td align="center">' . $final . '</td>';?>
                      <?php echo '<td style="font-size: 15px;" align="left">' . $name . '</td>';?>
                      <?php echo '<td style="font-size: 15px;" align="left">' . $row['Course'] . '</td>'; ?>
                    <td> 
                    <select name="pick[]">
                      <option value="None" <?php echo ($value == "0")?"selected":"" ?> >None</option>
                      <option value="Beadle" <?php echo ($value == "2")?"selected":"" ?> >Beadle</option>
                      <option value="Co-Beadle" <?php echo ($value == "1")?"selected":"" ?> >Co-Beadle</option>
                    </select>
                    </td>
                    </tr>
                    </tbody>
                    <?php $i++; } ?>
                </table>
                <?php echo $this->session->flashdata('error_update'); ?> <!-- Display Error Message -->
                <div align="center">
                <input type="submit" id="assign" name="assign" class="btn btn-primary" value="Assign"/>
                <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>" class="btn btn-primary">Cancel</a>
              </form>
              </div>
            </div>
          </div>

        </div>
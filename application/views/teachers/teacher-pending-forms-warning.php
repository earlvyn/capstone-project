 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

           <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
             </li>
            <li class="breadcrumb-item active">Warning Pending Forms</li>
          </ol>


          <ul class="navbar-nav ml-auto ml-md-0">
           <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <button class="btn btn-primary">Form Type</button>
          </a>
          <div class="dropdown-menu dropdown-menu-left" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?php echo base_url('teachers/teachers_pending_forms_daily?code=' . $code); ?>">Daily Attendance Monitoring Form</a>
            <a class="dropdown-item" href="<?php echo base_url('teachers/teachers_pending_forms_warning?code=' . $code); ?>">Notice of Warning on Attendance</a>
            <a class="dropdown-item" href="#">Notice of AF on Attendance</a>
        </li>
      </ul>

          <?php 
            $code = $_GET['code'];

            $sql = $this->db->query("SELECT * from subjects WHERE ClassCode='$code'");

            $result = $sql->row();
          ?>

          <!-- DataTables Example Notice of Warning on Attendance -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
            <b>Notice of Warning on Attendance </b><?php echo '<b style="float: right; font-size: 20px;">Allowable Abscences: ' . $result->max_absent . '</b>'; ?></div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" align="center">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>ID Number</th>
                      <th>Name</th>
                      <th>No. of Absent</th>
                      <th>View Record</th>
                    </tr>
                  </thead>
                 <tbody class="table-body" align="center">
                  <?php 
                  date_default_timezone_set('Asia/Manila');
                  $code = $_GET['code'];
                  $id = $this->session->userdata('username');

                  //$date = date("Y-m-d");
                  $date = date("F j, Y (l)");

                  $result = $this->db->query("SELECT * from subjects WHERE ClassCode='$code' AND Teacher_FK='$id'");

                  $q = $result->row();

                  $count = $q->max_absent;

                  $i = 1;
                  

                  $sql = $this->db->query("SELECT * From forms WHERE ClassCode='$code'");

                  $query = $this->db->query("SELECT distinct Students_fk From forms WHERE ClassCode='$code'");

                  foreach($query->result_array() as $row)
                  {

                  $idn = $row['Students_fk'];
                  $w = 0;
                  foreach($sql->result_array() as $rows)
                  {
                      if($rows['Students_fk'] == $idn)
                      {
                        if($rows['Remark'] == 'Absent')
                        {
                          $w++;
                        }
                      }
                  }

                  $year = substr($idn,1,4); //To get the year
                  $mid = substr($idn,-5,-4); //To get the 5th character
                  $last = substr($idn,6,4); //To get the last 4 character

                  $final = $year . '-' . $mid . '-' . $last; // e.g 20131-1-0183 

                  $qwe = $this->db->query("SELECT * FROM students WHERE Student_ID='$idn'");

                  $rec = $qwe->row();

                  $fname = $rec->First_Name;
                  $mname = substr($rec->Middle_Name,0,1);
                  $lname = $rec->Last_Name;

                  $name = '<b>' . $lname . '</b>, ' . $fname . ' ' . $mname . '.';

                  $ppw = $this->db->query("SELECT * FROM forms WHERE ClassCode='$code' AND Students_fk='$idn' AND Form_Type='warning' AND osa_status='2'");

                  $awe = $ppw->row();


                  if($ppw->num_rows() == 0)
                  {

                  }
                  else
                  {
                  if($w == $count){
                  ?>
                  <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $final; ?></td>
                  <td><?php echo $name; ?></td>
                  <td><?php echo $w; ?></td>
                  <td align="center"><a href="<?php echo base_url('teachers/teachers_pending_forms_warning_view?code=' . $code . '&id=' . $idn . '&num=' . $w); ?>"><i class="fas fa-eye"> View</i></a></td>
                  
                  </tr>
                  <?php $i++; }}}?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
          
          

        
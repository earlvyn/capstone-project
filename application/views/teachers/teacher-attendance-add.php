 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

           <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('teachers/teachers_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('teachers/teachers_attendance_option?code=' . $code); ?>">Select View</a>
            </li>
             </li>
            <li class="breadcrumb-item active">List View</li>
          </ol>

          <?php 
            date_default_timezone_set('Asia/Manila');
            $date = date("F j, Y (l)");
            
            echo "<a class='subjectn' style='float: left; margin-right: 80px; font-size: 20px;'>$date</a>";

            $code = $_GET['code'];

            $sql = $this->db->query("SELECT * from subjects WHERE ClassCode='$code'");

            $result = $sql->row();

            ?>
            <br />
            <br />
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Add Attendance Record(List View) <?php echo '<b style="float: right; font-size: 20px;">Allowable Abscences: ' . $result->max_absent . '</b>'; ?></div>
            <div class="card-body">
              <div class="table-responsive">
                <?php 
                $code = $_GET['code'];
                $sql = $this->db->query("SELECT * FROM classes WHERE ClassCode_Fk='$code'");
                if($sql->num_rows() > 0) {
                ?>
                <form action="<?php echo base_url('teachers/record_attendance?code=' . $code); ?>" method="POST">
                <table class="table table-bordered" width="100%" cellspacing="0">
                  <thead class="table-heading">
                  <tr>
                  <th></th>
                  <th>ID No</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Remark</th>
                  </tr>
                  </thead>
                  
                  <tbody class="table-body" align="center">
                  <?php
                  $code = $_GET['code'];
              
                  $result = $this->db->query("SELECT CONCAT(First_Name,' ', Last_Name) AS name, Course, Student_ID FROM students, classes WHERE Students_FK=Student_ID AND ClassCode_FK='$code' ORDER BY Last_Name ASC");
                  
                  $i = 0;
                  $x = 1;

                  foreach($result->result_array() as $row)
                  {
                    $id = $row['Student_ID'];
                    $year = substr($id,1,4); //To get the year
                    $mid = substr($id,-5,-4); //To get the 5th character
                    $last = substr($id,6,4); //To get the last 4 character

                    $final = $year . '-' . $mid . '-' . $last; // e.g 20131-1-0183
                    echo '<tr>';
                    echo '<td>' . $x . '</td>';
                    echo '<td>' . $final . '</td>';
                    echo '<td>' . $row['name'] . '</td>';

                    $sql = $this->db->query("SELECT max_absent from subjects WHERE ClassCode='$code'");

                    $q = $sql->row();

                    $count = $q->max_absent;

                    $query = $this->db->query("SELECT * from attendance_record WHERE ClassCode_fk='$code'");
                    $w = 0;
                    foreach($query->result_array() as $try)
                    {
                        if($try['Students_fk'] == $id)
                        {
                            if($try['Status'] == 'Absent' )
                            {
                              $w++;
                            }
                        }
                    }
                  ?>
                  <?php  if($w == $count+1) {?>
                  <td>
                  <input type="hidden" name="Status<?php echo $i; ?>" value="AF"/>
                  </td>
                  <?php 
                  } else { ?>

                  <td>
                  <input type="radio" name="Status<?php echo $i; ?>" value="Present" checked> Present
                  <input type="radio" name="Status<?php echo $i; ?>" value="Absent" > Absent
                  <input type="radio" name="Status<?php echo $i; ?>" value="Late"> Late
                  </td>
                  <?php
                  } 
                  if($w == $count+1)
                  {
                    echo '<td style="background-color: red;">AF</td>';
                  }
                  elseif($w == $count)
                  {
                    echo '<td style="background-color: yellow;">Warning</td>';
                  }
                  else
                  {
                    echo '<td style="background-color: white;"></td>';
                  }

                  ?>
                  <?php $i++; $x++; } ?>
                  <?php echo '</tr>'; ?>
                  <tbody>
                    
                  </table>
                  <div align="center">
                  <input type ="submit" name="insert" value="Save" class="btn btn-success"/>
                </div>
                </form>
              <?php } else { echo '<td> Currently No Students Enrolled!! </td>'; }?>
              </div>
            </div>
          </div>

        </div>
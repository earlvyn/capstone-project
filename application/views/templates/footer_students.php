<!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>© AdNU ATTENDANCE MONITORING SYSTEM 2018</span>
            </div>
          </div>
        </footer>

        

          </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

      <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url('assets/sb-admin/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
     <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('assets/sb-admin/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
    <!-- Page level plugin JavaScript-->
    <script src="<?php echo base_url('assets/sb-admin/vendor/chart.js/Chart.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/sb-admin/vendor/datatables/jquery.dataTables.js'); ?>"></script>
    <script src="<?php echo base_url('assets/sb-admin/vendor/datatables/dataTables.bootstrap4.js'); ?>"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('assets/sb-admin/js/sb-admin.min.js'); ?>"></script>
    <!-- Demo scripts for this page-->
    <script src="<?php echo base_url('assets/sb-admin/js/demo/datatables-demo.js'); ?>"></script>
    <script src="<?php echo base_url('assets/sb-admin/js/demo/chart-area-demo.js'); ?>"></script>

    
   
</body>
</html>
<!DOCTYPE html>
<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>AAMS</title>
    <!-- Bootstrap core CSS-->
    <link href="<?php echo base_url('assets/sb-admin/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url('assets/sb-admin/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="<?php echo base_url('assets/sb-admin/vendor/datatables/dataTables.bootstrap4.css')?>" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('assets/sb-admin/css/sb-admin.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/sb-admin/css/semester.css'); ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

   </head>

    <body id="page-top" >

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="<?php echo base_url('students/students_class'); ?>">AAMS</a>
      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>
      

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#">
            <i class="fas fa-user"></i>
            <span>Hi <?php echo $username; ?>!</span>
          </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo base_url('osa/osa_class'); ?>">
            <i class="far fa-file-alt"></i>
            <span>Forms</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('login/logout'); ?>">
            <i class="fas fa-power-off"></i>
            <span>Log Out</span>
          </a>
        </li>
     </ul>
 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

          <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('students/students_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">Attendance - <?php echo $s_code; ?></li>
          </ol>

           <a href="<?php echo base_url('students/students_list?code=' . $code); ?>" class="btn btn-success"><i class="fas fa-th-list"></i> Classlist</a>
           <a href="<?php echo base_url('students/students_sp?code=' . $code); ?>" class="btn btn-success"><i class="fas fa-layer-group"></i> Seat Plan</a>
            <br />
          <br />
         
         

       
           <!-- DataTables Example -->
          <div class="card mb-3" >
            <div class="card-header" >
              <i class="fas fa-table" ></i>
              My Attendance</div>
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>Date</th>
                      <th>Remark</th>
                      <th>Excuse Letter</th>
                      <td>Action</td>
                    </tr>
                  </thead>
                  <tbody class="table-body" align="center">
                  <?php 
                  $id = $this->session->userdata('username'); 
                  
                  $code = $_GET['code'];

                  $i = 0;

                  $query = $this->db->query("SELECT * FROM attendance_record where ClassCode_fk = '$code' AND Students_fk='$id'");

                  foreach($query->result_array() as $rows)
                  {
                    $i++;
                    
                    $stat = $rows['Status'];
                    $date = $rows['Date'];
                    $curr = date("F j, Y (l)", strtotime($date));
                    $ex = $rows['Excuse_Letter'];

                  ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $curr; ?> </td>
                      <td><?php echo $stat; ?></td>
                      <?php 
                      if($stat == 'Absent')
                      {
                        if(empty($ex))
                        {
                      ?>
                      <form method="POST" action="<?php echo base_url('students/upload_btn');?>?code=<?php echo $_GET['code']; ?>&date=<?php echo $date; ?>" enctype="multipart/form-data">
                      <input type="hidden" name="size" value="1000000">
                      <td>
                         <input type="file" name="image">
                        </td>
                        <td>
                        <input type="submit" id="upload" name="upload">
                         </td>
                        </form>

                      <?php } else {
                      ?>
                      <td><a href="<?php echo base_url('images\\' . $ex); ?>" target="_blank" onclick="window.open('images\\' . '$ex', 'popup', 'height=800, width=800'); return false;"><?php echo $ex; ?></a></td>
                      <td><i class="fas fa-check" style="color: green;"> Sent</i></td>
                     <?php }} 
                     else 
                      {  
                        echo '<td> None </td>';
                        echo '<td> None </td>';
                      }
                       ?>



                    </tr> 
                   <?php } ?>
                     </tbody>
                </table>
              </div>
            </div>
         
          </div>

        </div>
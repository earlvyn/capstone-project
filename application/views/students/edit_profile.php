<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--  This file has been downloaded from https://bootdey.com  -->
    <!--  All snippets are MIT license https://bootdey.com/license -->
    <title>AAMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editprofile.css">
</head>
<body>
<div class="container bootstrap snippets">
<div class="row">
  <div class="col-xs-12 col-sm-9">
      <?php echo form_open('students/update', "class='form-horizontal'"); ?>
      <div class="panel panel-default">
        <div class="panel-heading">
		<img src="<?php echo base_url('images/seal.png'); ?>" class="seal">
		<br />
		<br />
		<br />
		<br />
    <br />
        <h4 class="panel-title">Update Profile</h4>
        </div>
        <div class="panel-body">
		  <div class="form-group">
            <label class="col-sm-2 control-label">First Name</label>
            <div class="col-sm-10">
              <input id="fname" name="fname" type="text" class="form-control" placeholder="e.g Jose Bryan">
            </div>
          </div>
		   <div class="form-group">
            <label class="col-sm-2 control-label">Middle Name</label>
            <div class="col-sm-10">
              <input id="mname" name="mname" type="text" class="form-control" placeholder="e.g Monares">
            </div>
          </div>
		   <div class="form-group">
            <label class="col-sm-2 control-label">Last Name</label>
            <div class="col-sm-10">
              <input id="lname" name="lname" type="text" class="form-control" placeholder="e.g Galvo">
            </div>
          </div>
		   <div class="form-group">
            <label class="col-sm-2 control-label">Year</label>
            <div class="col-sm-10">
              <input class="form-control" id="year" name="year" type="number" min="1" max="5">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Course</label>
            <div class="col-sm-10">
              <select name="course" class="form-control" size="1">
                <option selected hidden value="#">Select Course</option>
								  <option>BS Accountancy</option>
								  <option>BS Business Administration Major in Business Economics</option>
								  <option>BS Business Administration Major in Business Engineering</option>
								  <option>BS Tourism major in Development Tourism Management</option>
								  <option>BS Entrepreneurship</option>
								  <option>BS Entrepreneurship major in Entrepreneurial Tourism</option>
								  <option>BS Business Administration Major in Legal Management</option>
								  <option>BS Business Administration Major in Banking and Finance</option>
									<option>BS Business Administration Major in Management</option>
									<option>BS Business Administration Major in Marketing Management</option>
									<option>BS Business Administration Major in Financial Management and Accounting</option>
                  <option>BS Business Administration Major in Computer Management and Accounting</option>
									<option value="BS Computer Science">BS Computer Science(BSCS)</option>
									<option value="BS Information Technology">BS Information Technology(BSIT)</option>
									<option value="BS Information Systems">BS Information Systems(BSIS)</option>
									<option>BS Digital Illustration and Animation</option> 
									<option>Bachelor of Science in Special in Education</option>
									<option>Bachelor of Secondary Education</option>
									<option>Bachelor in Library Information Science</option>
									<option>Bachelor in Physical Education</option>
									<option>AB Broadcasting</option>
									<option>AB Communication</option>
									<option>AB Journalism</option>
									<option>AB Economics</option>
									<option>AB Political Science</option>
									<option>AB English</option>
									<option>AB Literature</option>
									<option>AB Philosophy</option>
									<option>AB Political Science</option> 
									<option>BS Computer Engineering (BSCoe)</option> 
									<option value="Bachelor of Engineering Technology major in Computer Engineering Technology">Bachelor of Engineering Technology major in Computer Engineering Technology(BET-CpET)</option>
									<option value="BS Civil Engineering">BS Civil Engineering (BSCE)</option> 
									<option>BS Environmental Science</option> 
									<option>BS Biology</option> 
									<option>BS Mathematics</option> 
									<option>BS Psychology</option> 
									<option value="BS Electronics Engineering">BS Electronics Engineering(BS EE)</option> 
									<option>BS Nursing</option> 
									<option>Bachelor of Elementary Education</option> 
              </select>
            </div>
          </div>
          <?php echo $this->session->flashdata('error_update'); ?>
			<div class="form-group1">
            <div class="col-sm-10 col-sm-offset-2">
              <!--<button id="update" name="submit" type="submit" class="btn btn-primary">Update</button>-->
              <?php echo form_submit('update', 'Update', "class='btn btn-primary'"); ?> 
              <a href="<?php echo base_url('login/logout'); ?>" class="btn btn-default">Cancel</a>
            </div>
          </div>	
        </div>
      </div>

          </div>
          
        </div>
      </div>
    <?php echo form_close(); ?>
  </div>
</div>



</body>
</html>


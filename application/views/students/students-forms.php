<div id="content-wrapper" style="background-color: #dfe3ee;">
        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb" >
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('students/students_forms'); ?>">My Forms</a>
            </li>

          </ol>

           <!-- DataTables Example -->
          <div class="card mb-3" >
            <div class="card-header" >
              <i class="fas fa-table" ></i>
              Received Forms</div>
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>Subject Code</th>
                      <th>Section</th>
                      <th>Subject Name</th>
                      <td>Form Type</td>
                      <td>Instructor</td>
                      <td>View</td>
                    </tr>
                  </thead>
                  <tbody class="table-body" align="center">
                 <?php 
                 $id = $this->session->userdata('username');

                 $sql = $this->db->query("SELECT * from forms WHERE Students_fk='$id' AND Form_Type='warning' OR Form_Type='af'");
                 $i = 1;
                 foreach($sql->result_array() as $rows)
                 {

                 	$date = $rows['Date'];
                 	$tk = $rows['Teacher_fk'];
                 	$code = $rows['ClassCode'];

                 	$query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

                 	$q = $query->row_array();
                 	
                 	$name = $this->db->get_where('teacher', array('Faculty_ID' => $tk))->row();

                 	$final = $name->First_Name . ' ' . $name->Last_Name;

                 	if($rows['Form_Type'] == 'warning')
                 	{
                 		$wa = 'Notice of Warning On Attendance';
                 	}
                 	else
                 	{
                 		$wa =  'Notice of AF On Attendance';
                 	}

                 	$sql1 = $this->db->query("SELECT * from forms WHERE Form_Type='daily' AND osa_status='1' AND ClassCode='$code' AND Teacher_fk='$tk'");

                 	 $w = 0;

                      foreach($sql1->result_array() as $row)
                      {
                      if($row['Students_fk'] == $id)
                      {
                        if($row['Remark'] == 'Absent')
                        {
                          $w++;
                        }
                      }
                      }

                 ?>
                 <td><?php echo $i; ?></td>	
                 <td><?php echo $q['Subject_Code']; ?></td>
                 <td><?php echo $q['Section']; ?></td>
                 <td><?php echo $q['Subject_Name']; ?></td>
                 <td><?php echo $wa; ?></td>
                 <td><?php echo $final; ?></td>
                 <?php 
                 if($rows['Form_Type'] == 'warning')
                 {

                 ?>
                 <td><a href="<?php echo base_url('students/students_forms_view?code=' . $code . '&id=' . $id . '&num=' . $w . '&date=' . $date); ?>"><i class="fas fa-eye"> View</i></a></td>
             	<?php 
             	} 
             	else
             	{
             	?>
             	 <td><a href="<?php echo base_url('students/students_forms_view1?code=' . $code . '&id=' . $id . '&num=' . $w . '&date=' . $date); ?>"><i class="fas fa-eye"> View</i></a></td>	
             	<?php 
            	 $i++; }} 
             	?>
                
                 </tbody>
                </table>
              </div>
            </div>
         
          </div>

        </div>
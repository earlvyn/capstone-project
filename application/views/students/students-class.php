 <div id="content-wrapper" style="background-color: #dfe3ee;">
        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb" >
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('students/students_class'); ?>">My Classes</a>
            </li>

          </ol>

          <button class="btn btn-primary" data-toggle = "modal" data-target = "#myModal">
					 Join a class
		  </button>

      <!-- Modal -->
<div class = "modal fade" id = "myModal" tabindex = "-1" role = "dialog" 
   aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <h3 class = "modal-title" id = "myModalLabel">
               Enter Subject Key
            </h3>
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
          
         </div>
         <div class = "modal-body">
       <?php echo form_open('students/add_class'); ?>
           <label class="subjectn"><b>Subject Key</b></label><br />
       <input type="text" name="code1" id="code1" maxlength="6" size="50" required><br /> 
        <?php if($this->session->flashdata('error_update') == TRUE) { ?> <!-- Automatically Open when error found-->
        <script type="text/javascript">
        $(window).on('load',function(){
          $('#myModal').modal('show');});
      </script>
      <?php } ?>
      <?php echo $this->session->flashdata('error_update'); ?> <!-- Display Error Message -->
          <br />
          <div class = "modal-footer">
            <button type= "submit" id="enroll" name="enroll" class = "btn btn-primary">
               Enroll
            </button>
            <button type="button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
           </div> 
    <?php echo form_close();?>
         
         </div>
    
         
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
  
</div><!-- /.modal -->

<br />
<br />

<div class="row">
  <?php
  
      $user = $this->session->userdata('username');
      
      $query = $this->db->query("SELECT * from subjects, classes WHERE Students_FK='$user' AND ClassCode_FK=ClassCode");
       
      foreach($query->result_array() as $row){
    
      $role = $row['role'];
      
      ?>
      <!-- Icon Cards-->
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-book-open"></i>
                  </div>
                  <?php if($role == '0') {?>
                  <div class="mr-5"><a style="font-size: 26px;" href="<?php echo base_url('students/students_attendance'); ?>?code=<?php echo $row['ClassCode_FK'];?>" class="text-white"><?php echo $row['Subject_Code'] . ' - ' . $row['Section']; ?></a></br><?php echo $row['Subject_Name']; ?></div>
                  <?php } ?>
                  <?php if($role == '2' || $role == '1') {?>
                  <div class="mr-5"><a style="font-size: 26px;" href="<?php echo base_url('beadle/beadle_attendance'); ?>?code=<?php echo $row['ClassCode_FK'];?>" class="text-white"><?php echo $row['Subject_Code'] . ' - ' . $row['Section']; ?></a></br><?php echo $row['Subject_Name']; ?></div>
                  <?php } ?>
                </div>
                <div class="card-footer text-white clearfix small z-1">
                  <?php if($role == '0') {?>
                  <span class="float-left" style="font-size: 15px;">Role: Student</span>
                  <?php } ?>
                  <?php if($role == '1') {?>
                  <span class="float-left" style="font-size: 15px;">Role: Co-Beadle</span>
                  <?php } ?>
                  <?php if($role == '2') {?>
                  <span class="float-left" style="font-size: 15px;"><b>Role</b>: Beadle</span>
                  <?php } ?>
                </div>
              </div>
            </div>
            
        
        <?php } ?>
        </div>
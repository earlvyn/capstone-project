<div id="content-wrapper" style="background-color: #dfe3ee;">
        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb" >
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('students/students_forms'); ?>">My Forms</a>
            </li>
 				<li class="breadcrumb-item active">View Form</li>
          </ol>

          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Notice of AF on Attendance</div>
            <div class="card-body">
              <div class="table-responsive">
                 
                <table border="1px solid black" align="center" width="60%">
                    <thead class="table-heading">
                    <tr>
                    <th colspan="2" style="text-align: center; height: 70px;">Notie of AF On Attendance</th>
                    </tr>
                    </thead>
                    <tbody class="table-body">
                    <?php
                     
                      $code = $_GET['code'];
                      $id = $_GET['id'];
                    
                      $sql1 = $this->db->query("SELECT `Date`, Teacher_fk from forms WHERE ClassCode='$code' AND Students_fk='$id' AND Form_Type='af'"); 

                      $waw = $sql1->row();

                      $date = $waw->Date;
                      $tk = $waw->Teacher_fk;
                      
                      $result = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

                      $sql = $this->db->query("SELECT * from students WHERE Student_ID='$id'");

                      $rec = $sql->row();

                      $fname = $rec->First_Name;
                      $mname = substr($rec->Middle_Name,0,1);
                      $lname = $rec->Last_Name;

                      $name = $fname . ' ' . $mname . '. ' . $lname;

                      $row = $result->row();

                      $curr = date("F j, Y (l)", strtotime($date));
                      ?>
                      <tr>
                        <td style="height: 40px;">&nbsp;<b>Name: </b><?php echo $name; ?></td>
                        <td style="height: 40px;">&nbsp;<b>Subject Code: </b><?php echo $row->Subject_Code;?></td>
                      </tr>
                       <tr>
                         <td colspan="0" style="height: 50px;">&nbsp;<b>Date: </b><?php echo $curr; ?></td>
                         <td style="height: 40px;">&nbsp;<b>Section: </b><?php echo $row->Section; ?></td>
                      </tr>
                    </tbody>
                </table>
                <table border="1px solid black" align="center" width="60%">
                  <tr>
                   <td class="table-body" colspan="3">
                    No. of Abscences: <b style="font-size: 20px; color: red;"><u><?php echo $_GET['num']; ?></u></b><br /><br />

                    Please be informed that you have exceeded the number of allowable abscences. Consquently you shall incur an AF grade in your subject and shall receive a notification letter from the office.

                    <br /><br />

                    For your guidance. <br /><br />

                    Certified Correct

                    
                  </td>
                  </tr> 
                </table>
                <table border="1px solid black" align="center" width="60%">
                <tr>
                <?php 
                $sqli = $this->db->query("SELECT * FROM teacher WHERE Faculty_ID='$tk'");

                $trya = $sqli->row();

                $nametk = $trya->First_Name . ' ' . $trya->Last_Name;
                ?>
                <th class="table-body" style="text-align: center;">Recorded/Certified by: <br/><u><?php echo $nametk; ?></u><br /><b style="font-size: 15px">Teacher</b><br /><b style="float: left;">OSA-SA-02 </b></th>
                </tr>
                </table>
              </div>
            </div>
          </div>

        </div>
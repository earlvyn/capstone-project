 <div id="content-wrapper" style="background-color: #dfe3ee;">
        
        <div class="container-fluid">

          <?php
        
          $code = $_GET['code'];

          $query = $this->db->query("SELECT * FROM subjects WHERE ClassCode='$code'");

          foreach($query->result_array() as $row)
          {
            $name = $row['Subject_Name'];
            $section = $row['Section'];
            $s_code = $row['Subject_Code'];
            $semester = $row['semester'];
            $syear = $row['syear'];
          }

            $final = $semester . ' School Year ' . $syear;
          ?>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo base_url('students/students_class'); ?>">My Classes - <?php echo $final;?></a>
            </li>
            <li class="breadcrumb-item active">
              <a href="<?php echo base_url('students/students_attendance?code=' . $code); ?>">Attendance - <?php echo $s_code; ?></a>
            </li>
             </li>
            <li class="breadcrumb-item active">Class List</li>
          </ol>

          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Class List View</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                  <thead class="table-heading">
                    <tr align="center">
                      <th></th>
                      <th>Name</th>
                      <th>Course</th>     
                    </tr>
                  </thead>
                      <?php
          
                    $code = $_GET['code'];
                
                    $result = $this->db->query( "SELECT * FROM students, classes WHERE Students_FK=Student_ID AND ClassCode_FK='$code' ORDER BY Last_Name ASC");
                    
                    $record = array();
                    $i=1;
                    foreach($result->result_array() as $row)
                    {
                      $record[] = $row;
                    }
                    
                    ?>
                    
                    <?php foreach($record as $rec) 
                    {
                      $fname = $rec['First_Name'];
                      $mname = substr($rec['Middle_Name'],0,1);
                      $lname = $rec['Last_Name'];

                      $name = '<b>' . $lname . '</b>, ' . $fname . ' ' . $mname . '.';

                     ?>
                    <tbody class="table-body">
                    <tr>
                    <?php echo '<td align="center"><i class="fas fa-user-circle" style="font-size: 24px;"></i></td>';?>
                    <?php echo '<td align="left">' . $name . '</td>';?>
                    <?php echo '<td align="left">' . $rec['Course'] . '</td>'; ?>
                    </tr>
                    </tbody>
                    <?php $i++; } ?>
                </table>
              </div>
            </div>
          </div>

        </div>
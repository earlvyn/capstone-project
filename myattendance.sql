-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 03:29 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myattendance`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_record`
--

CREATE TABLE `attendance_record` (
  `record_id` int(11) NOT NULL,
  `Date` date NOT NULL,
  `time` varchar(50) NOT NULL,
  `Status` varchar(40) NOT NULL,
  `ClassCode_fk` varchar(6) NOT NULL,
  `Students_fk` varchar(10) NOT NULL,
  `Teacher_fk` varchar(10) NOT NULL,
  `Excuse_Letter` varchar(30) NOT NULL,
  `Recorded_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `attendance_record`
--

INSERT INTO `attendance_record` (`record_id`, `Date`, `time`, `Status`, `ClassCode_fk`, `Students_fk`, `Teacher_fk`, `Excuse_Letter`, `Recorded_by`) VALUES
(1, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201520012', 'p201310183', '', 'p201310183'),
(2, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201310193', 'p201310183', '', 'p201310183'),
(3, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201310088', 'p201310183', '', 'p201310183'),
(4, '2018-10-13', '10:21:20 AM', 'Absent', 'SRixlG', 's201311114', 'p201310183', '', 'p201310183'),
(5, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201510249', 'p201310183', '', 'p201310183'),
(6, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201510917', 'p201310183', '', 'p201310183'),
(7, '2018-10-13', '10:21:20 AM', 'Absent', 'SRixlG', 's201410187', 'p201310183', '', 'p201310183'),
(8, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201410013', 'p201310183', '', 'p201310183'),
(9, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201310183', 'p201310183', '', 'p201310183'),
(10, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201510884', 'p201310183', '', 'p201310183'),
(11, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201411365', 'p201310183', '', 'p201310183'),
(12, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201310123', 'p201310183', '', 'p201310183'),
(13, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201510382', 'p201310183', '', 'p201310183'),
(14, '2018-10-13', '10:21:20 AM', 'Late', 'SRixlG', 's201410829', 'p201310183', '', 'p201310183'),
(15, '2018-10-13', '10:21:20 AM', 'Present', 'SRixlG', 's201511221', 'p201310183', '', 'p201310183'),
(16, '2018-10-13', '10:21:20 AM', 'Late', 'SRixlG', 's201310206', 'p201310183', '', 'p201310183'),
(17, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201520012', 'p201310183', '', 'p201310183'),
(18, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201310193', 'p201310183', '', 'p201310183'),
(19, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201310088', 'p201310183', '', 'p201310183'),
(20, '2018-10-10', '2:34:13 PM', 'Absent', 'SRixlG', 's201311114', 'p201310183', '', 'p201310183'),
(21, '2018-10-10', '2:34:13 PM', 'Absent', 'SRixlG', 's201510249', 'p201310183', 'Tulips.jpg', 'p201310183'),
(22, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201510917', 'p201310183', '', 'p201310183'),
(23, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201410187', 'p201310183', '', 'p201310183'),
(24, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201410013', 'p201310183', '', 'p201310183'),
(25, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201310183', 'p201310183', '', 'p201310183'),
(26, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201510884', 'p201310183', '', 'p201310183'),
(27, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201411365', 'p201310183', '', 'p201310183'),
(28, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201310123', 'p201310183', '', 'p201310183'),
(29, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201510382', 'p201310183', '', 'p201310183'),
(30, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201410829', 'p201310183', '', 'p201310183'),
(31, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201511221', 'p201310183', '', 'p201310183'),
(32, '2018-10-10', '2:34:13 PM', 'Present', 'SRixlG', 's201310206', 'p201310183', '', 'p201310183'),
(33, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201520012', 'p201310183', '', 'p201310183'),
(34, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201310193', 'p201310183', '', 'p201310183'),
(35, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201310088', 'p201310183', '', 'p201310183'),
(36, '2018-10-11', '8:29:33 PM', 'Absent', 'SRixlG', 's201311114', 'p201310183', '', 'p201310183'),
(37, '2018-10-11', '8:29:33 PM', 'Absent', 'SRixlG', 's201510249', 'p201310183', '', 'p201310183'),
(38, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201510917', 'p201310183', '', 'p201310183'),
(39, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201410187', 'p201310183', '', 'p201310183'),
(40, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201410013', 'p201310183', '', 'p201310183'),
(41, '2018-10-11', '8:29:33 PM', 'Absent', 'SRixlG', 's201310183', 'p201310183', 'Chrysanthemum.jpg', 'p201310183'),
(42, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201510884', 'p201310183', '', 'p201310183'),
(43, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201411365', 'p201310183', '', 'p201310183'),
(44, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201310123', 'p201310183', '', 'p201310183'),
(45, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201510382', 'p201310183', '', 'p201310183'),
(46, '2018-10-11', '8:29:33 PM', 'Present', 'SRixlG', 's201410829', 'p201310183', '', 'p201310183'),
(47, '2018-10-11', '8:29:33 PM', 'Absent', 'SRixlG', 's201511221', 'p201310183', '', 'p201310183'),
(48, '2018-10-11', '8:29:33 PM', 'Absent', 'SRixlG', 's201310206', 'p201310183', '', 'p201310183'),
(49, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201520012', 'p201310183', '', 'p201310183'),
(50, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201310193', 'p201310183', '', 'p201310183'),
(51, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201310088', 'p201310183', '', 'p201310183'),
(52, '2018-10-14', '1:53:59 PM', 'Absent', 'SRixlG', 's201311114', 'p201310183', '', 'p201310183'),
(53, '2018-10-14', '1:53:59 PM', 'Absent', 'SRixlG', 's201510249', 'p201310183', '', 'p201310183'),
(54, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201510917', 'p201310183', '', 'p201310183'),
(55, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201410187', 'p201310183', '', 'p201310183'),
(56, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201410013', 'p201310183', '', 'p201310183'),
(57, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201310183', 'p201310183', '', 'p201310183'),
(58, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201510884', 'p201310183', '', 'p201310183'),
(59, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201411365', 'p201310183', '', 'p201310183'),
(60, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201310123', 'p201310183', '', 'p201310183'),
(61, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201510382', 'p201310183', '', 'p201310183'),
(62, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201410829', 'p201310183', '', 'p201310183'),
(63, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201511221', 'p201310183', '', 'p201310183'),
(64, '2018-10-14', '1:53:59 PM', 'Present', 'SRixlG', 's201310206', 'p201310183', '', 'p201310183');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` int(11) NOT NULL,
  `ClassCode_FK` varchar(6) NOT NULL,
  `Students_FK` varchar(10) NOT NULL,
  `Teacher_FK` varchar(10) NOT NULL,
  `role` int(11) NOT NULL,
  `seat_no` int(11) NOT NULL,
  `arow` int(11) NOT NULL,
  `acol` int(11) NOT NULL,
  `brow` int(11) NOT NULL,
  `bcol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`class_id`, `ClassCode_FK`, `Students_FK`, `Teacher_FK`, `role`, `seat_no`, `arow`, `acol`, `brow`, `bcol`) VALUES
(1, 'SRixlG', 's201310183', 'p201310183', 2, 1, 4, 5, 4, 5),
(2, 'SRixlG', 's201411365', 'p201310183', 0, 2, 4, 5, 4, 5),
(3, 'SRixlG', 's201520012', 'p201310183', 0, 11, 4, 5, 4, 5),
(4, 'SRixlG', 's201310206', 'p201310183', 0, 4, 4, 5, 4, 5),
(5, 'SRixlG', 's201511221', 'p201310183', 0, 5, 4, 5, 4, 5),
(6, 'SRixlG', 's201410187', 'p201310183', 0, 25, 4, 5, 4, 5),
(7, 'SRixlG', 's201310123', 'p201310183', 0, 7, 4, 5, 4, 5),
(8, 'SRixlG', 's201510382', 'p201310183', 0, 8, 4, 5, 4, 5),
(9, 'SRixlG', 's201410829', 'p201310183', 0, 9, 4, 5, 4, 5),
(10, 'SRixlG', 's201510249', 'p201310183', 0, 10, 4, 5, 4, 5),
(11, 'SRixlG', 's201310088', 'p201310183', 0, 20, 4, 5, 4, 5),
(12, 'SRixlG', 's201310193', 'p201310183', 0, 40, 4, 5, 4, 5),
(13, 'SRixlG', 's201410013', 'p201310183', 0, 22, 4, 5, 4, 5),
(14, 'SRixlG', 's201311114', 'p201310183', 0, 23, 4, 5, 4, 5),
(15, 'SRixlG', 's201510884', 'p201310183', 0, 24, 4, 5, 4, 5),
(16, 'SRixlG', 's201510917', 'p201310183', 0, 30, 4, 5, 4, 5),
(17, 'AFpqdi', 's201310183', 'p201310183', 0, 3, 4, 5, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `forms_id` int(11) NOT NULL,
  `Date` date NOT NULL,
  `ClassCode` varchar(6) NOT NULL,
  `Remark` varchar(20) NOT NULL,
  `Form_Type` varchar(100) NOT NULL,
  `Teacher_fk` varchar(10) NOT NULL,
  `Students_fk` varchar(10) NOT NULL,
  `Status` varchar(20) NOT NULL,
  `osa_status` int(11) NOT NULL,
  `Recorded_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`forms_id`, `Date`, `ClassCode`, `Remark`, `Form_Type`, `Teacher_fk`, `Students_fk`, `Status`, `osa_status`, `Recorded_by`) VALUES
(153, '2018-10-10', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201311114', 'Approved', 1, 'p201310183'),
(154, '2018-10-10', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201510249', 'Approved', 1, 'p201310183'),
(155, '2018-10-11', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201311114', 'Approved', 1, 'p201310183'),
(156, '2018-10-11', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201510249', 'Approved', 1, 'p201310183'),
(157, '2018-10-11', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201310183', 'Approved', 1, 'p201310183'),
(158, '2018-10-11', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201511221', 'Approved', 1, 'p201310183'),
(159, '2018-10-11', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201310206', 'Approved', 1, 'p201310183'),
(160, '2018-10-13', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201311114', 'Approved', 1, 'p201310183'),
(161, '2018-10-13', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201410187', 'Approved', 1, 'p201310183'),
(162, '2018-10-13', 'SRixlG', 'Late', 'daily', 'p201310183', 's201410829', 'Approved', 1, 'p201310183'),
(163, '2018-10-13', 'SRixlG', 'Late', 'daily', 'p201310183', 's201310206', 'Approved', 1, 'p201310183'),
(164, '2018-10-14', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201311114', 'Approved', 1, 'p201310183'),
(165, '2018-10-14', 'SRixlG', 'Absent', 'daily', 'p201310183', 's201510249', 'Approved', 1, 'p201310183'),
(166, '2018-10-15', 'SRixlG', 'Warning', 'warning', 'p201310183', 's201510249', 'Approved', 3, 'p201310183');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `SID` int(11) NOT NULL,
  `Student_ID` varchar(10) NOT NULL,
  `First_Name` varchar(30) NOT NULL,
  `Middle_Name` varchar(30) NOT NULL,
  `Last_Name` varchar(30) NOT NULL,
  `Year` int(11) NOT NULL,
  `Course` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`SID`, `Student_ID`, `First_Name`, `Middle_Name`, `Last_Name`, `Year`, `Course`, `Email`) VALUES
(1, 's201310183', 'Jbskie', 'Monares', 'Galvo', 4, 'BS Information Technology', 'jgalvo@gbox.adnu.edu.ph'),
(2, 's201411365', 'James Patrick', 'Bulawan', 'Montanez', 4, 'BS Information Technology', 'jmontanez@gbox.adnu.edu.ph'),
(3, 's201520012', 'Dranreed Xavier', 'Badrina', 'Abias', 3, 'AB Communication', 'dabias@gbox.adnu.edu.ph'),
(4, 's201310206', 'Brent Daniel', 'Agar', 'Verganio', 5, 'BS Accountancy', 'bverganio@gbox.adnu.edu.ph'),
(5, 's201511221', 'Levi John', 'Patrocinio', 'Sta Ana', 4, 'BS Computer Science', 'lstaana@gbox.adnu.edu.ph'),
(6, 's201410187', 'Carlo James', 'Bordado', 'Carino', 3, 'BS Information Technology', 'cjcarino@gbox.adnu.edu.ph'),
(7, 's201310123', 'Manelle', 'Ella', 'Moral', 3, 'BS Psychology', 'mmoral@gbox.adnu.edu.ph'),
(8, 's201510382', 'Shierra', 'Corral', 'Nishiyama', 3, 'BS Information Technology', 'snishiyama@gbox.adnu.edu.ph'),
(9, 's201410829', 'Josh Michael', 'Pelayo', 'Olea', 4, 'BS Information Technology', 'jmolea@gbox.adnu.edu.ph'),
(10, 's201510249', 'Xenia', 'Baldemoro', 'Bracero', 4, 'BS Information Technology', 'xbracero@gbox.adnu.edu.ph'),
(11, 's201310088', 'Shiela Joy', 'Bajar', 'Balona', 4, 'BS Business Administration Major in Financial Management and Accounting', 'sjbalona@gbox.adnu.edu.ph'),
(12, 's201310193', 'Jerome Louie', 'Ballesteros', 'Badiola', 5, 'BS Accountancy', 'jbadiola@gbox.adnu.edu.ph'),
(13, 's201410013', 'Paul Carmelo', 'Samillano', 'Cunanan', 4, 'BS Information Technology', 'pcunanan@gbox.adnu.edu.ph'),
(14, 's201311114', 'Davild Dragoljub', 'Baldemor', 'Bichara', 4, 'BS Entrepreneurship', 'dbichara@gbox.adnu.edu.ph'),
(15, 's201510884', 'Marie Danielle', 'None', 'Licmoan', 3, 'BS Business Administration Major in Computer Management and Accounting', 'mlicmoan@gbox.adnu.edu.ph'),
(16, 's201510917', 'Czerince', 'Tipo', 'Brioso', 3, 'BS Accountancy', 'cbrioso@gbox.adnu.edu.ph'),
(17, 's201510183', '', '', '', 0, '', '12345@gbox.com');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `Subject_ID` int(11) NOT NULL,
  `Subject_Code` varchar(30) NOT NULL,
  `Subject_Name` varchar(40) NOT NULL,
  `Section` varchar(30) NOT NULL,
  `ClassCode` varchar(6) NOT NULL,
  `Teacher_FK` varchar(10) NOT NULL,
  `max_absent` int(11) NOT NULL,
  `semester` varchar(30) NOT NULL,
  `syear` varchar(30) NOT NULL,
  `room` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`Subject_ID`, `Subject_Code`, `Subject_Name`, `Section`, `ClassCode`, `Teacher_FK`, `max_absent`, `semester`, `syear`, `room`) VALUES
(1, 'ICST240', 'Operating Systems', 'N1', 'SRixlG', 'p201310183', 3, '2nd Semester', '2018-2019', 'AL211'),
(2, 'ICST101', 'Web Development and Programming', 'N3', 'AFpqdi', 'p201310183', 6, '2nd Semester', '2018-2019', 'P216');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `FID` int(11) NOT NULL,
  `Faculty_ID` varchar(10) NOT NULL,
  `First_Name` varchar(30) NOT NULL,
  `Middle_Name` varchar(30) NOT NULL,
  `Last_Name` varchar(30) NOT NULL,
  `Email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`FID`, `Faculty_ID`, `First_Name`, `Middle_Name`, `Last_Name`, `Email`) VALUES
(1, 'p201310183', 'Jose Bryan', 'Monares', 'Galvo', '1@gbox.com'),
(2, 'p201410183', '', '', '', '123@gbox.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(30) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `username`, `password`, `role`) VALUES
(1, 'p201310183', '123', 1),
(2, 'p201410183', '123', 1),
(3, 's201310183', '123', 0),
(4, 's201411365', '123', 0),
(5, 's201520012', '123', 0),
(6, 's201310206', '123', 0),
(7, 's201511221', '123', 0),
(8, 's201410187', '123', 0),
(9, 's201310123', '123', 0),
(10, 's201510382', '123', 0),
(11, 's201410829', '123', 0),
(12, 's201510249', '123', 0),
(13, 's201310088', '123', 0),
(14, 's201310193', '123', 0),
(15, 's201410013', '123', 0),
(16, 's201311114', '123', 0),
(17, 's201510884', '123', 0),
(18, 's201510917', '123', 0),
(19, 'osa', 'test', 2),
(20, 's201510183', '123', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance_record`
--
ALTER TABLE `attendance_record`
  ADD PRIMARY KEY (`record_id`),
  ADD KEY `ClassCode_fk` (`ClassCode_fk`),
  ADD KEY `Students_fk` (`Students_fk`),
  ADD KEY `Teacher_fk` (`Teacher_fk`),
  ADD KEY `Date` (`Date`),
  ADD KEY `Status` (`Status`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`),
  ADD KEY `ClassCode_FK` (`ClassCode_FK`),
  ADD KEY `Students_FK` (`Students_FK`),
  ADD KEY `Teacher_FK` (`Teacher_FK`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`forms_id`),
  ADD KEY `ClassCode` (`ClassCode`),
  ADD KEY `Date` (`Date`),
  ADD KEY `Teacher_fk` (`Teacher_fk`),
  ADD KEY `Students_fk` (`Students_fk`),
  ADD KEY `Remark` (`Remark`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`SID`),
  ADD KEY `Student_ID` (`Student_ID`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`Subject_ID`),
  ADD KEY `ClassCode` (`ClassCode`),
  ADD KEY `Teacher_FK` (`Teacher_FK`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`FID`),
  ADD KEY `Faculty_ID` (`Faculty_ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`),
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance_record`
--
ALTER TABLE `attendance_record`
  MODIFY `record_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `forms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `SID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `Subject_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `FID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance_record`
--
ALTER TABLE `attendance_record`
  ADD CONSTRAINT `attendance_record_ibfk_1` FOREIGN KEY (`ClassCode_fk`) REFERENCES `classes` (`ClassCode_FK`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `attendance_record_ibfk_2` FOREIGN KEY (`Students_fk`) REFERENCES `classes` (`Students_FK`);

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`Students_FK`) REFERENCES `students` (`Student_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `classes_ibfk_2` FOREIGN KEY (`ClassCode_FK`) REFERENCES `subjects` (`ClassCode`),
  ADD CONSTRAINT `classes_ibfk_3` FOREIGN KEY (`Teacher_FK`) REFERENCES `subjects` (`Teacher_FK`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forms`
--
ALTER TABLE `forms`
  ADD CONSTRAINT `forms_ibfk_1` FOREIGN KEY (`ClassCode`) REFERENCES `attendance_record` (`ClassCode_fk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forms_ibfk_3` FOREIGN KEY (`Teacher_fk`) REFERENCES `attendance_record` (`Teacher_fk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forms_ibfk_4` FOREIGN KEY (`Students_fk`) REFERENCES `attendance_record` (`Students_fk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`Student_ID`) REFERENCES `users` (`username`);

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `subjects_ibfk_1` FOREIGN KEY (`Teacher_FK`) REFERENCES `teacher` (`Faculty_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`Faculty_ID`) REFERENCES `users` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
